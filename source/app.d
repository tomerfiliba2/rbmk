module app;

import rbmk.logging;


void f(int x) {mixin(traced);
	DEBUG!"aaa %s"(123);
	DEBUG!"bbb %s %s"(11, 12);
	g(x*2, 7);
}

enum E {
	A, B, C, D
}

struct S {
	int x;
	int y;
	E z;
}

int g(int x, int y) {mixin(traced);
	DEBUG!"ccc %s"(S(x, y, E.C));
	//throw new Exception("foo");
	return x+y;
}

import rbmk.logging.logdesc: LogDesc;
extern(C) void emitLog(const(LogDesc)* desc, const(ubyte)[] argsBuf) @safe nothrow @nogc {
	import rbmk.logging.console_emitter: consoleEmitter;
	consoleEmitter(desc, argsBuf);
}


void main() {mixin(traced);
	f(18);
}
