module rbmk.arch.x86;

version(X86_64):

enum CACHE_LINE_SIZE = 64;

version(LDC) {
    public import ldc.intrinsics: readTSC = llvm_readcyclecounter;
}
else version (D_InlineAsm_X86_64) {
    ulong readTSC() pure nothrow @nogc @safe {
        asm pure nothrow @nogc @safe {
            naked;
            rdtsc;         // EDX(hi):EAX(lo)
            shl RDX, 32;
            or RAX, RDX;   // RAX |= (RDX << 32)
            ret;
        }
    }
}

unittest {
    assert (readTSC() != 0);
}

version(LDC) {
    public import ldc.intrinsics: xchg = llvm_atomic_rmw_xchg;
}
else version (D_InlineAsm_X86_64) {
    ulong xchg(shared ulong* ptr /*RSI*/, ulong val /*RDI*/) pure nothrow @nogc @safe {
        asm pure nothrow @nogc @safe {
            naked;
            //lock; -- xchg has an implicit LOCK prefix
            xchg [RSI], RDI;
            mov RAX, RDI;
            ret;
        }
    }
}

version(LDC) {
    public import ldc.intrinsics: xadd = llvm_atomic_rmw_add;
}
else version (D_InlineAsm_X86_64) {
    ulong xadd(shared ulong* ptr /*RSI*/, long delta /*RDI*/) pure nothrow @nogc @safe {
        asm pure nothrow @nogc @safe {
            naked;
            lock;
            xadd [RSI], RAX;
            mov RAX, RDI;
            ret;
        }
    }
}

version (LDC) {
    void prefetch(const void* p) pure nothrow @trusted @nogc {
        pragma(inline, true);
        import ldc.intrinsics: llvm_prefetch;
        llvm_prefetch(cast(void*)p, 0 /*read*/, 3 /*very local*/, 1 /*data*/);
    }
}
else version (D_InlineAsm_X86_64) {
    void prefetch(const void* p /* RDI */) pure nothrow @safe @nogc {
        pragma(inline, true);
        asm pure nothrow @safe @nogc {
            naked;
            prefetcht0 [RDI];
            ret;
        }
    }
}

void prefetch(uint NUM_CACHE_LINES)(const void* p) pure nothrow @trusted @nogc {
    static foreach(i; 0 .. NUM_CACHE_LINES) {
        prefetch(p + i*CACHE_LINE_SIZE);
    }
}

unittest {
    // prefetching will not segfault
    prefetch(null);

    ulong[1000] x = void;
    prefetch(&x[800]);
}
