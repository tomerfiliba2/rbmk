module rbmk.arch.linux;

version(linux):
version(X86_64) {
    enum NR_gettid = 186;
    enum NR_tgkill = 234;
}

// this is defined as an enum so we can do things in CTFE with it; we assert in runtime it's indeed the value
enum SYS_PAGE_SIZE = 4096;

shared static this() {
    import core.sys.posix.unistd: sysconf, _SC_PAGESIZE;
    import std.string: format;
    import std.exception: enforce;
    long sz = sysconf(_SC_PAGESIZE);
    enforce(SYS_PAGE_SIZE == sz, "page size is %s, expected %s".format(sz, SYS_PAGE_SIZE));
}

extern(C) nothrow @nogc {
    long syscall(int number, ...) @system;

    int gettid() @trusted {
        return cast(int)syscall(NR_gettid);
    }

    int tgkill(int tgid, int tid, int sig) @trusted {
        return cast(int)syscall(NR_tgkill, tgid, tid, sig);
    }
}
