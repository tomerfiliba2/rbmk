module rbmk.containers.arrays;

import rbmk.lib.types: CapacityType, setRefToInit;
import rbmk.lib.asserts: ASSERT;


struct FixedArray(T, size_t N) {
    enum CapacityType!N capacity = N;
    CapacityType!N _length;
    T[N] elements;

    @property inout(T)[] slice() pure nothrow @nogc @safe {
        return elements[0 .. _length];
    }
    @property immutable(T)[] islice() const pure nothrow @nogc @safe {
        return elements[0 .. _length];
    }
    alias slice this;

    @property size_t length() const pure nothrow @nogc {
        return _length;
    }
    @property void length(size_t newLength) nothrow @nogc {
        ASSERT!"newLength (%s) >= capacity (%s)"(newLength < capacity, newLength, capacity);
        foreach(ref e; elements[length .. newLength]) {
            setRefToInit(e);
        }
        _length = newLength;
    }

    void opOpAssign(string op: "~")(T elem) nothrow @nogc {
        ASSERT!"array is full (%s)"(_length < capacity, length);
        elements[_length] = elem;
        _length++;
    }
}

alias FixedString(size_t N) = FixedArray(char, N);
