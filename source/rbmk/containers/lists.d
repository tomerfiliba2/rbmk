module rbmk.containers.lists;

import rbmk.lib.asserts: ASSERT, DBG_ASSERT;


struct IntrusiveList(T, string _chainAttr = null, LengthType=void) {
    static if(_chainAttr is null) {
        enum chainAttr = "";
    }
    else {
        enum chainAttr = "." ~ _chainAttr;
    }
    alias Ptr = typeof(mixin("T" ~ chainAttr ~ ".next"));
    static assert (is(Ptr == typeof(mixin("T" ~ chainAttr ~ ".prev"))));
    enum supportsAnchoring = __traits(hasMember, mixin("T" ~ chainAttr), "isAnchor");
    static if (supportsAnchoring) {
        enum supportNotifyFrontRemoval = __traits(hasMember, mixin("T" ~ chainAttr), "notifyFrontRemoval");
    } else {
        enum supportNotifyFrontRemoval = false;
    }

    private Ptr _head;
    static if (!is(LengthType == void)) {
        private LengthType _length;
    }

    static if (is(T* == Ptr)) {
        enum simplePointer = true;

        private static T* fromPtr(Ptr x) nothrow @nogc {pragma(inline, true);
            return x;
        }
    }
    else {
        enum simplePointer = false;

        private static T* fromPtr(Ptr x) nothrow @nogc {pragma(inline, true);
            return x ? &(*x) : null;
        }
    }

    @property T* head() nothrow @nogc {pragma(inline, true);
        return fromPtr(_head);
    }

    @property bool _empty() const pure nothrow @nogc {pragma(inline, true);
        return _head ? false : true;
    }

    static if (!is(LengthType == void)) {
        @property bool empty() const nothrow @nogc {
            return _length == 0;
        }
    }
    else static if (supportsAnchoring) {
        @property bool empty() const nothrow @nogc {
            auto self = cast(IntrusiveList*)&this;
            return _empty || (isAnchor(self.head) && self.getNextRealOf(self.head) is null);
        }
    }
    else {
        alias empty = _empty;
    }

    @property T* tail() nothrow @nogc {
        return _head ? getPrevOf(_head) : null;
    }
    static if (!is(LengthType == void)) {
        @property LengthType length() const pure nothrow @nogc {pragma(inline, true);
            return _length;
        }
    }

    static bool isLinked(P)(P p) nothrow @nogc {
        DBG_ASSERT!"%s: p=%s n=%s"((getNextOf(p) !is null) == (getPrevOf(p) !is null), p, getPrevOf(p), getNextOf(p));
        return getNextOf(p) !is null;
    }

    private static T* getNextOf(P)(P p) nothrow @nogc {return fromPtr(mixin("(*p)" ~ chainAttr ~ ".next"));}
    private static T* getPrevOf(P)(P p) nothrow @nogc {return fromPtr(mixin("(*p)" ~ chainAttr ~ ".prev"));}
    private static void setNextOf(P1, P2)(P1 p, P2 newNext) nothrow @nogc {mixin("(*p)" ~ chainAttr ~ ".next = newNext;");}
    private static void setPrevOf(P1, P2)(P1 p, P2 newPrev) nothrow @nogc {mixin("(*p)" ~ chainAttr ~ ".prev = newPrev;");}

    void pushHead(T* elem) nothrow @nogc {
        ASSERT!"%s: prev=%s next=%s"(getNextOf(elem) is null && getPrevOf(elem) is null, elem, getPrevOf(elem), getNextOf(elem));
        if (_head) {
            T* oldHead = head;
            T* oldTail = tail;
            setPrevOf(oldHead, elem);
            setNextOf(elem, oldHead);
            setPrevOf(elem, oldTail);
            setNextOf(oldTail, elem);
            _head = elem;
        }
        else {
            setPrevOf(elem, elem);
            setNextOf(elem, elem);
            _head = elem;
        }
        static if (!is(LengthType == void)) {
            if (!isAnchor(elem)) {
                _length++;
            }
        }
    }

    void pushTail(T* elem) nothrow @nogc {
        ASSERT!"%s: prev=%s next=%s"(getNextOf(elem) is null && getPrevOf(elem) is null, elem, getPrevOf(elem), getNextOf(elem));
        if (_head) {
            T* oldTail = tail;
            setNextOf(oldTail, elem);
            setPrevOf(elem, oldTail);
            setNextOf(elem, head);
            setPrevOf(head, elem);
        }
        else {
            _head = elem;
            setNextOf(_head, elem);
            setPrevOf(_head, elem);
        }
        static if (!is(LengthType == void)) {
            if (!isAnchor(elem)) {
                _length++;
            }
        }
    }

    alias prepend = pushHead;
    alias append = pushTail;

    static if(supportsAnchoring) {
        void insertAfter(T* elem, T* after) nothrow @nogc {
            auto nextReal = getNextRealOf(after);
            if(nextReal is null) {
                pushTail(elem);
                return;
            }
            insertBefore(elem, nextReal);
        }
    }
    else {
        alias insertAfter = _insertAfter;
    }

    void _insertAfter(T* elem, T* after) nothrow @nogc {
        ASSERT!"%s: prev=%s next=%s empty=%s after=%s"(elem !is null && after !is null && getNextOf(elem) is null && getPrevOf(elem) is null && !_empty,
            elem, getPrevOf(elem), getNextOf(elem), _empty, after);

        auto oldNext = getNextOf(after);
        DBG_ASSERT!"%s: oldNext is null after=%s head=%s"(oldNext !is null, elem, after, head);
        setNextOf(after, elem);
        setPrevOf(elem, after);
        setNextOf(elem, oldNext);
        setPrevOf(oldNext, elem);
        static if (!is(LengthType == void)) {
            if (!isAnchor(elem)) {
                _length++;
            }
        }
    }

    void insertBefore(T* elem, T* before) nothrow @nogc {
        ASSERT!"%s: prev=%s next=%s empty=%s before=%s"(elem !is null && before !is null && getNextOf(elem) is null && getPrevOf(elem) is null && !_empty,
            elem, getPrevOf(elem), getNextOf(elem), _empty, before);

        auto oldPrev = getPrevOf(before);
        DBG_ASSERT!"%s: oldPrev is null before=%s head=%s"(oldPrev !is null, elem, before, head);
        setNextOf(oldPrev, elem);
        setPrevOf(elem, oldPrev);
        setNextOf(elem, before);
        setPrevOf(before, elem);
        static if (!is(LengthType == void)) {
            if (!isAnchor(elem)) {
                _length++;
            }
        }
    }

    T* remove(T* elem) nothrow @nogc {
        T* n = getNextOf(elem);
        T* p = getPrevOf(elem);
        ASSERT!"head=%s elem=%s n=%s p=%s"(!_empty && n !is null && p !is null, head, elem, n, p);

        if (getNextOf(head) is head) {
            // single element
            DBG_ASSERT!""(elem is head);
            _head = null;
        }
        else {
            setNextOf(p, n);
            setPrevOf(n, p);
            if (elem is head) {
                _head = n;
            }
        }
        setNextOf(elem, null);
        setPrevOf(elem, null);

        static if (supportNotifyFrontRemoval) {
            if (!isAnchor(elem)) {
                while (n.isAnchor) {
                    n.notifyFrontRemoval();
                    n = getNextOf(n);
                    if (n is null) break;
                }
            }
        }
        static if (!is(LengthType == void)) {
            if (!isAnchor(elem)) {
                DBG_ASSERT!"_length=0"(_length > 0);
                _length--;
            }
        }
        return elem;
    }

    T* popHead() nothrow @nogc {return _empty ? null : remove(head);}
    T* popTail() nothrow @nogc {return _empty ? null : remove(tail);}

    void removeAll() nothrow @nogc {
        while (!_empty) {remove(head);}
    }

    void splice(ref IntrusiveList listToDestroy) nothrow @nogc {
        if (listToDestroy._empty) {
            return;
        }

        scope(exit) listToDestroy = IntrusiveList.init;
        if (_empty) {
            this = listToDestroy;
            return;
        }
        auto head1 = head;
        auto tail1 = tail;
        auto head2 = listToDestroy.head;
        auto tail2 = listToDestroy.tail;

        setNextOf(tail1, head2);
        setPrevOf(head2, tail1);
        setNextOf(tail2, head1);
        setPrevOf(head1, tail2);

        static if (!is(LengthType == void)) {
            _length += listToDestroy.length;
        }
    }

    static if (supportsAnchoring) {
        private static bool isAnchor(P)(P p) nothrow @nogc {
            return mixin("(*p)" ~ chainAttr ~ ".isAnchor") ? true : false;
        }
    }
    else {
        pragma(inline, true)
        private static bool isAnchor(P)(P p) nothrow @nogc {return false;}
    }

    private static T* followingRealOf(alias direction)(T* elem, T* sentinel) nothrow @nogc {
        if (elem is null) {
            return null;
        }
        assert (sentinel !is null);
        while (true) {
            elem = direction(elem);
            if (elem is sentinel) {
                return null;
            }
            if (!isAnchor(elem)) {
                return elem;
            }
        }
    }

    private T* getNextRealOf(T* elem) nothrow @nogc { return followingRealOf!getNextOf(elem, head); }
    private T* getPrevRealOf(T* elem) nothrow @nogc { return followingRealOf!getPrevOf(elem, tail); }

    //
    // a range that supports the front being removed, i.e.
    //
    //     foreach(elem; list.range) {
    //         list.remove(elem);
    //     }
    //
    static struct Range(string dir) {
        import std.algorithm: among;
        static assert(dir.among("forward", "backward"));
        enum isForward = dir == "forward";
        IntrusiveList* list;
        T* front;
        private T* nextFront;

        private void setNextFront(T* prev) {
            static if(isForward) {
                nextFront = list.getNextRealOf(prev);
            } else {
                nextFront = list.getPrevRealOf(prev);
            }
        }

        this(IntrusiveList* list) nothrow @nogc {
            this.list = list;
            if (list._empty) {
                front = null;
                nextFront = null;
            }
            else {
                auto sentinel = isForward ? list.head : list.tail;
                front = isAnchor(sentinel) ? list.getNextRealOf(sentinel) : sentinel;
                setNextFront(sentinel);
            }
        }

        @property bool empty() const pure nothrow @nogc {
            return front is null || list._empty;
        }

        void popFront() nothrow @nogc {
            front = nextFront;
            setNextFront(nextFront);
        }
    }

    auto range() nothrow @nogc {
        return Range!"forward"(&this);
    }

    auto range() const nothrow @nogc {
        return Range!"forward"(cast(IntrusiveList*)&this);
    }

    auto reversedRange() nothrow @nogc {
        return Range!"backward"(&this);
    }

    static if (supportsAnchoring) {
        //
        // a range that supports arbitrary changes to the list (i.e., parallel modifications while iterating)
        // by adding an anchor element from which iteration starts
        //
        static struct AnchoredRange {
            IntrusiveList* list;
            T* anchor;

            @disable this(this);

            this(IntrusiveList* list, T* anchor) nothrow @nogc {
                DBG_ASSERT!"anchor is null"(anchor !is null);
                DBG_ASSERT!"anchor already linked"(!isLinked(anchor));
                DBG_ASSERT!"anchor not marked as anchor"(isAnchor(anchor));

                this.list = list;
                this.anchor = anchor;
                auto first = list.head;
                while (first !is null) {
                    if (!isAnchor(first)) {
                        list._insertAfter(anchor, first);
                        break;
                    }
                    first = getNextOf(anchor);
                    if (first is list.head) {
                        break;
                    }
                }
            }
            ~this() nothrow @nogc {
                if (isLinked(anchor)) {
                    list.remove(anchor);
                }
            }

            @property bool empty() nothrow @nogc {
                return front is null;
            }

            @property T* front() nothrow @nogc {
                if (!isLinked(anchor) || list._empty) {
                    return null;
                }
                auto curr = anchor;
                while (curr !is list.head) {
                    curr = getPrevOf(curr);
                    if (!isAnchor(curr)) {
                        return curr;
                    }
                }
                return null;
            }

            void popFront() nothrow @nogc {
                if (!isLinked(anchor) || list._empty) {
                    return;
                }
                auto next = list.getNextRealOf(anchor);
                list.remove(anchor);
                if (next !is null) {
                    list._insertAfter(anchor, next);
                }
            }
        }

        auto anchoredRange(T* anchor) nothrow @nogc {
            return AnchoredRange(&this, anchor);
        }
    }

    version(unittest) private string debugDump() {
        auto curr = head;
        auto tail = this.tail;
        import std.stdio;
        import std.string;
        writef("[");
        while (curr !is null) {
            static if (__traits(hasMember, T, "toString")) {
                auto s = curr.toString();
            }
            else {
                auto s = "%s".format(curr);
            }
            if (isAnchor(curr)) {
                writef("A<%s>,", s);
            }
            else {
                writef("%s,", s);
            }
            curr = getNextOf(curr);
            if (curr is head) {
                break;
            }
        }
        writeln("]");
        return null;
    }
}

struct IndexPointer(T, IdxType, alias fromIdx, alias toIdx, IdxType invalid=IdxType.max) {
    IdxType idx = invalid;

    @property bool isValid() const pure nothrow @nogc {pragma(inline, true);
        return idx != invalid;
    }
    bool opCast(Q: bool)() const pure nothrow @nogc {pragma(inline, true);
        return idx != invalid;
    }
    void opAssign(IndexPointer idxPtr) nothrow @nogc {pragma(inline, true);
        idx = idxPtr.idx;
    }
    ref T opUnary(string op: "*")() nothrow @nogc {pragma(inline, true);
        return fromIdx(idx);
    }
    void opAssign(T* p) nothrow @nogc {pragma(inline, true);
        idx = p is null ? invalid : toIdx(p);
    }
}

version(unittest):
private:

import std.stdio;
import std.string;
import std.algorithm: map;
import std.array: array;

ref S SfromIdx(uint idx) nothrow @nogc {return sArr[idx];}
uint StoIdx(S* p) nothrow @nogc {return cast(uint)(p - sArr.ptr);}
alias SPtr = IndexPointer!(S, uint, SfromIdx, StoIdx);

static struct SPtrChain {
    SPtr next, prev;
}
static struct PtrChain {
    S* next, prev;
}
static struct AnchoredChain {
    bool isAnchor;
    S* next, prev;
}

static struct S {
    int value;
    SPtrChain chain1;
    PtrChain chain2;
    PtrChain chain3;
    AnchoredChain chain4;

    string toString() {
        return "%s".format(value);
    }
}

__gshared static S[] sArr;

unittest {
    foreach(i; 0 .. 10) {sArr ~= S(i);}
}

unittest {
    struct NextPrevOnly {
        NextPrevOnly* prev, next;
    }
    bool validate() {
        NextPrevOnly a;
        IntrusiveList!NextPrevOnly list;
        list.pushTail(&a);
        assert(list.popHead() is &a);
        return true;
    }
    static assert(validate());
}

unittest {
    IntrusiveList!(S, "chain1", uint) list;
    IntrusiveList!(S, "chain2") list2;
    IntrusiveList!(S, "chain3") list3;

    static assert (!list.supportsAnchoring);
    static assert (!list2.supportsAnchoring);
    static assert (!list3.supportsAnchoring);

    assert (list.empty);
    assert (list2.empty);

    foreach(i; 0 .. 5) {
        assert (list.length == i);
        list.pushTail(&sArr[i]);
        assert (list.length == i + 1);
        list2.pushHead(&sArr[i]);
    }

    assert (!list.empty);
    assert (!list2.empty);

    foreach(i; 5 .. 10) {
        assert (list.length == i);
        list.pushHead(&sArr[i]);
        assert (list.length == i + 1);
        list2.pushTail(&sArr[i]);
    }

    auto a = array(map!"a.value"(list.range));
    assert (a == [9, 8, 7, 6, 5, 0, 1, 2, 3, 4]);

    list.remove(&sArr[5]);
    list.remove(&sArr[6]);
    list.remove(&sArr[0]);
    list.remove(&sArr[1]);

    a = array(map!"a.value"(list.range));
    assert (a == [9, 8, 7, 2, 3, 4]);

    list.removeAll();
    assert (list.empty);

    a = array(map!"a.value"(list2.range));
    assert (a == [4, 3, 2, 1, 0, 5, 6, 7, 8, 9]);

    assert (list3.empty);
    list3.pushHead(&sArr[0]);
    list3.pushHead(&sArr[1]);
    list3.pushTail(&sArr[2]);

    a = array(map!"a.value"(list3.range));
    assert (a == [1, 0, 2]);

    list.removeAll();
    a = array(map!"a.value"(list.range));
    assert (list.length == 0);
    assert (a == []);

    list.pushTail(&sArr[0]);
    list.pushTail(&sArr[1]);
    assert (list.length == 2);
    a = array(map!"a.value"(list.range));
    assert (a == [0, 1]);

    list.popHead();
    a = array(map!"a.value"(list.range));
    assert (list.length == 1);
    assert (a == [1]);

    list.removeAll();
    list.pushTail(&sArr[0]);
    list.pushTail(&sArr[1]);
    list.remove(&sArr[1]);
    assert (list.length == 1);
    assert (list.head is &sArr[0]);
    a = array(map!"a.value"(list.range));
    assert (a == [0], "%s".format(a));

    list.removeAll();
    typeof(list) secondList;
    list.pushTail(&sArr[0]);
    list.pushTail(&sArr[1]);
    list.pushTail(&sArr[2]);
    list.pushTail(&sArr[3]);

    secondList.pushTail(&sArr[4]);
    secondList.pushTail(&sArr[5]);
    secondList.pushTail(&sArr[6]);
    secondList.pushTail(&sArr[7]);

    list.splice(secondList);
    assert (secondList.empty);
    assert (list.length == 8);

    a = array(map!"a.value"(list.range));
    assert (a == [0, 1, 2, 3, 4, 5, 6, 7]);

    // a no-op splice
    list.splice(secondList);
    assert (secondList.empty);
    assert (list.length == 8);

    // reverse splice
    secondList.splice(list);
    assert (list.empty);
    assert (secondList.length == 8);

    secondList.insertAfter(&sArr[9], &sArr[2]);
    a = array(map!"a.value"(secondList.range));
    assert (a == [0, 1, 2, 9, 3, 4, 5, 6, 7]);
    assert (secondList.length == 9);

    secondList.insertBefore(&sArr[8], &sArr[5]);
    assert (secondList.length == 10);
    a = array(map!"a.value"(secondList.range));
    assert (a == [0, 1, 2, 9, 3, 4, 8, 5, 6, 7]);
    secondList.removeAll();

    list.removeAll();
    foreach(ref e; sArr) {
        list.append(&e);
    }
    assert (list.length == 10);
    foreach(s; list.range) {
        if (s.value % 2 == 0) {
            list.remove(s);
        }
    }
    assert (list.length == 5);
    a = array(map!"a.value"(list.range));
    assert (a == [1, 3, 5, 7, 9]);
}

unittest {
    S anchor1;
    anchor1.value = 8888;
    anchor1.chain4.isAnchor = true;
    S anchor2;
    anchor2.chain4.isAnchor = true;
    anchor2.value = 9999;

    IntrusiveList!(S, "chain4") list4;
    static assert (list4.supportsAnchoring);

    {
        auto ar1 = list4.anchoredRange(&anchor1);
        auto ar2 = list4.anchoredRange(&anchor2);
        //assert (!list4._empty);
        assert(ar1.empty);
        assert(ar2.empty);
    }

    {
        assert (list4._empty);
        foreach(ref e; sArr) {
            list4.pushTail(&e);
        }

        auto a = array(map!"a.value"(list4.range));
        assert (a == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);

        auto ar1 = list4.anchoredRange(&anchor1);
        auto ar2 = list4.anchoredRange(&anchor2);

        a = array(map!"a.value"(list4.range));
        assert (a == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);

        void popNext(R, E...)(ref R rng, E expected) {
            auto tmp = rng.front();
            static if (E.length == 0) {
                assert (tmp is null, "popped %s when expecting empty %s".format(tmp.value, list4.debugDump));
                assert (rng.empty);
            }
            else {
                //writefln("popped %s", tmp.value);
                assert (tmp.value == expected[0], "got %s expected %s %s".format(tmp.value, expected[0], list4.debugDump));
            }
            rng.popFront();
        }

        popNext(ar1, 0);

        popNext(ar2, 0);
        popNext(ar2, 1);
        popNext(ar2, 2);

        a = array(map!"a.value"(list4.range));
        assert (a == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);

        list4.remove(&sArr[3]);

        popNext(ar1, 1);
        popNext(ar1, 2);
        popNext(ar1, 4);
        popNext(ar1, 5);

        popNext(ar2, 2);
        popNext(ar2, 4);

        list4.remove(&sArr[6]);
        list4.remove(&sArr[7]);

        popNext(ar1, 5);
        popNext(ar1, 8);
        popNext(ar1, 9);
        popNext(ar1);

        list4.pushTail(&sArr[3]);

        popNext(ar2, 5);
        popNext(ar2, 8);
        popNext(ar2, 9);
        popNext(ar2, 3);
        popNext(ar2);
    }

    {
        list4.removeAll();
        assert (list4._empty);
        foreach(ref e; sArr) {
            list4.pushTail(&e);
        }

        auto a = array(map!"a.value"(list4.range));
        assert (a == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);

        uint[] b;

        foreach(x; list4.anchoredRange(&anchor1)) {
            b ~= x.value;

            if (x.value % 3 == 0) {
                list4.remove(x);
            }
        }

        assert (b == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], "%s".format(b));

        a = array(map!"a.value"(list4.range));
        assert (a == [1, 2, 4, 5, 7, 8]);
    }

}

version(unittest) {
    static struct Chain {
        bool isAnchor;
        Chain* prev, next;
        @nogc nothrow
        void notifyFrontRemoval() {
            assert(isAnchor);
            (cast(Anchor*)&this).invalidated = true;
        }
    }
    static struct Real {
        Chain chain;
        int i;
        this(int _i) { i = _i; }
    }
    static struct Anchor {
        Chain chain = Chain(true);
        bool invalidated;
    }
}
unittest {
    IntrusiveList!Chain list;
    auto first = Real(1);
    list.pushTail(&first.chain);
    // list.pushTail(&new Real(2).chain);
    // list.pushTail(&new Real(3).chain);
    Anchor anchor;
    auto iter = list.anchoredRange(&anchor.chain);
    assert(iter.front == &first.chain);

    auto second = Real(2);
    list.insertAfter(&second.chain, &first.chain);

    assert(iter.front == &first.chain);
    assert(!anchor.invalidated);
    iter.popFront();

    assert(iter.front == &second.chain);
    assert(!anchor.invalidated);

    auto third = Real(3);
    list.insertAfter(&third.chain, &second.chain);
    list.remove(&second.chain);
    assert(anchor.invalidated);
    anchor.invalidated = false;
    assert(!iter.empty);
    iter.popFront();

    assert(iter.front == &third.chain);
    iter.popFront();

    assert(iter.empty);
}

unittest {
    struct Elem { Elem* prev, next; }
    Elem a, b;
    IntrusiveList!Elem list;
    list.pushTail(&a);
    list.pushTail(&b);
    import std.algorithm: equal;
    assert(list.range.equal([&a, &b]));
    assert(list.reversedRange.equal([&b, &a]));
}
