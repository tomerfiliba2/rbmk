module rbmk.containers.pools;

import rbmk.lib.memory: MmapArray;


struct DefaultOptions {
    alias IDX=uint;
    alias RC=uint;
}

struct Freelist(T, Options = DefaultOptions) {
    align(1) static struct Element {
    align(1):
        Options.IDX nextIdx;
        static if (!is(Options.RC == void)) {
            Options.RC refcount;
        }
        ubyte[T.sizeof] data;
    }

    void initialize(T[] elements) {
    }
}

struct MmapPool(T) {
    MmapArray buffer;

    void allocate(size_t capacity) {
        buffer.allocate(T.sizeof * capacity);
    }
    void free() {
    }

}

struct FixedPool(T, size_t N) {
    enum capacity = N;
}
