module rbmk.lib.bits;

import core.bitop;

ubyte intLog2(size_t val) pure nothrow @nogc @safe {
    if (__ctfe) {
        ubyte i;
        for (i = 0; val > 1; i++) {
            val >>= 1;
        }
        return i;
    }
    else {
        return val <= 1 ? 0 : cast(ubyte)bsr(val);
    }
}
