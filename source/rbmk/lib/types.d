module rbmk.lib.types;

import std.string: format;
import std.traits;
import std.meta;


pragma(inline, true) ref auto accessMember(string name, T)(auto ref T obj) pure nothrow @nogc @safe {
    return obj.tupleof[staticIndexOf!(name, FieldNameTuple!T)];
}

alias accessMemberType(T, string name) = typeof(T.tupleof[staticIndexOf!(name, FieldNameTuple!T)]);

template CapacityType(size_t val) {
    static if (val <= ubyte.max) {
        alias CapacityType = ubyte;
    }
    else static if (val <= ushort.max) {
        alias CapacityType = ushort;
    }
    else static if (val <= uint.max) {
        alias CapacityType = uint;
    }
    else {
        alias CapacityType = ulong;
    }
}

template bitFlag(alias field, ubyte offset) {
    @property bool bitFlag() const pure nothrow @nogc {
        return (field & (1UL << offset)) != 0;
    }
    @property void bitFlag(bool val) pure nothrow @nogc {
        if (val) {
            field |= 1UL << offset;
        }
        else {
            field &= ~(1UL << offset);
        }
    }
}

template bitField(T, alias field, ubyte offset, ubyte width) {
    static assert (width >= 1);
    enum mask = (1 << width) - 1;
    static if (is(T == enum)) {
        static assert (T.min >= 0);
        static assert (T.max <= mask);
    }
    @property T bitField() const pure nothrow @nogc {
        return cast(T)((field >> offset) & mask);
    }
    @property void bitField(T val) pure nothrow @nogc {
        const bits = ((cast(ulong)val) & mask) << offset;
        field = (field & ~(mask << offset)) | bits;
    }
}

unittest {
    import std.string;

    static struct S {
        ushort x;
        alias flag1 = bitFlag!(x, 0);
        alias flag2 = bitFlag!(x, 1);
        alias flag3 = bitFlag!(x, 2);
        alias flag4 = bitFlag!(x, 3);
    }

    S s;
    s.flag1 = true;
    s.flag4 = true;
    assert (s.flag1);
    assert (!s.flag2);
    assert (!s.flag3);
    assert (s.flag4);
    assert ("%b".format(s.x) == "1001");
}

template isVersion(string NAME) {
    mixin("version (" ~ NAME ~") {enum isVersion = true;} else {enum isVersion=false;}");
}

ubyte[] asBytes(T)(ref T val) nothrow @nogc if (!isPointer!T) {pragma(inline, true);
    static assert( !hasElaborateDestructor!T, "Cannot convert to bytes a type with destructor" );
    return (cast(ubyte*)&val)[0 .. T.sizeof];
}
const(ubyte)[] asBytes(T)(const ref T val) nothrow @nogc if (!isPointer!T) {pragma(inline, true);
    static assert( !hasElaborateDestructor!T, "Cannot convert to bytes a type with destructor" );
    return (cast(ubyte*)&val)[0 .. T.sizeof];
}

T roundUp(alias N, T)(T num) pure nothrow @nogc {
    return ((num + (N-1)) / N) * N;
}
T roundDown(alias N, T)(T num) pure nothrow @nogc {
    return (num / N) * N;
}

unittest {
    static assert (roundDown!64(128) == 128);
    static assert (roundDown!64(129) == 128);
    static assert (roundUp!64(127) == 128);
    static assert (roundUp!64(128) == 128);
    static assert (roundUp!64(129) == 192);
}

// setting x = x.init duplicates x.init on the stack, which can cause segfaults when we're on fibers.
// this function avoids that
void setRefToInit(T)(ref T val) nothrow @trusted @nogc if (!isPointer!T) {
    auto initBuf = cast(ubyte[])typeid(T).initializer();
    if (initBuf.ptr is null) {
        val.asBytes[] = 0;
    }
    else {
        // duplicate static arrays to work around https://issues.dlang.org/show_bug.cgi?id=16394
        static if (isStaticArray!T) {
            foreach(ref e; val) {
                e.asBytes[] = initBuf;
            }
        }
        else {
            val.asBytes[] = initBuf;
        }
    }
}

void setPtrToInit(T)(T* val) @trusted @nogc nothrow {
    setRefToInit(*val);
}

struct TypedIdentifier(string name, T, T invalid_=T.max, T initVal = invalid_) if (isIntegral!T) {
    enum invalid = TypedIdentifier(invalid_);

    T value = initVal;
}
