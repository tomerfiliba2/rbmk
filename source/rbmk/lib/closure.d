module rbmk.lib.closure;

import std.traits;

struct Closure {
    enum ARGS_SIZE = 128 - 16; // so it would fit in 2 cache lines
    static assert (this.sizeof == 128);

private:
    enum DIRECT_FN = cast(void function(Closure*))0x1; // impossible value for a pointer
    enum DIRECT_DG = cast(void function(Closure*))0x2; // impossible value for a pointer

    void function(Closure*) _wrapper;
    void* _funcptr;
    union {
        void delegate()  _dg;
        ubyte[ARGS_SIZE] argsBuf;
    }

public:
    @property const(void)* funcptr() pure const nothrow @nogc {return _funcptr;}
    @property bool isSet() pure const nothrow @nogc {return _funcptr !is null;}
    bool opCast(T: bool)() const pure @nogc nothrow @safe {return _funcptr !is null;}
    ref Closure opAssign(typeof(null)) pure @nogc nothrow @safe {clear(); return this;}
    ref Closure opAssign(void function() fn) pure @nogc nothrow @safe {set(fn); return this;}
    ref Closure opAssign(void delegate() dg) pure @nogc nothrow @safe {set(dg); return this;}

    void clear() pure nothrow @nogc @safe {
        _wrapper = null;
        _funcptr = null;
        argsBuf[] = 0;
    }

    void opCall() {
        if (_funcptr is null) {
            // maybe assert?
            return;
        }
        else if (_wrapper == DIRECT_FN) {
            (cast(void function())_funcptr)();
        }
        else if (_wrapper == DIRECT_DG) {
            _dg();
        }
        else {
            _wrapper(&this);
        }
    }

    private template _check(Fs...) {
        static assert (is(ReturnType!(Fs[0]) == void));
        static foreach (i, sc; ParameterStorageClassTuple!(Fs[0])) {
            static assert (sc == ParameterStorageClass.none, "Parameters %s has an invalid storage class %s".format(i+1, sc));
        }
        enum _check = true;
    }

    void set(F, T...)(F f, T args) pure nothrow @nogc @trusted if (isFunctionPointer!F) {
        enum checked = _check!F;
        static assert (is(typeof(f(args))));

        static if (T.length == 0) {
            _wrapper = DIRECT_FN;
            _funcptr = f;
            argsBuf[] = 0;
        }
        else {
            // important: keep native alignment for GC
            struct Typed {
                staticMap!(Unqual, Parameters!F) args;
            }
            static void wrapper(Closure* closure) {
                (cast(F)closure._funcptr)((cast(Typed*)closure.argsBuf.ptr).args);
            }

            _funcptr = f;
            _wrapper = &wrapper;
            static assert (Typed.sizeof <= argsBuf.sizeof, "Args too big");
            static foreach(i; 0 .. args.length) {
                (cast(Typed*)argsBuf.ptr).args[i] = args[i];
            }
            argsBuf[Typed.sizeof .. $] = 0;
        }
    }

    void set(D, T...)(D dg, T args) pure nothrow @nogc @trusted if (isDelegate!D) {
        enum checked = _check!D;
        static assert (is(typeof(dg(args))));

        static if (Parameters!D.length == 0) {
            _wrapper = DIRECT_DG;
            _funcptr = dg.funcptr;
            _dg = dg;
            argsBuf[_dg.sizeof .. $] = 0;
        }
        else {
            // important: keep native alignment for GC
            struct Typed {
                D dg;
                staticMap!(Unqual, Parameters!D) args;
            }

            static void wrapper(Closure* closure) {
                auto typed = cast(Typed*)closure.argsBuf.ptr;
                typed.dg(typed.args);
            }

            _wrapper = &wrapper;
            _funcptr = dg.funcptr;
            static assert (Typed.sizeof <= argsBuf.sizeof, "Args too big");
            auto typed = cast(Typed*)argsBuf.ptr;
            typed.dg = dg;
            static foreach(i; 0 .. args.length) {
                typed.args[i] = args[i];
            }
            argsBuf[Typed.sizeof .. $] = 0;
        }
    }

    void set(alias F)(Parameters!F args) pure nothrow @nogc @trusted {
        enum checked = _check!F;

        static if (Parameters!F.length == 0) {
            _wrapper = DIRECT_FN;
            _funcptr = &F;
            argsBuf[] = 0;
        }
        else {
            // important: keep native alignment for GC
            struct Typed {
                staticMap!(Unqual, Parameters!F) args;
            }

            static void wrapper(Closure* closure) {
                F((cast(Typed*)closure.argsBuf.ptr).args);
            }

            _wrapper = &wrapper;
            _funcptr = &F;
            static assert (Typed.sizeof <= argsBuf.sizeof, "Args too big");
            (cast(Typed*)argsBuf.ptr).args = args;
            argsBuf[Typed.sizeof .. $] = 0;
        }
    }
}

unittest {
    import std.string;
    Closure c;

    static string[] res;
    int localVar;

    static void f() {
        res ~= "f";
    }
    static void g(int x, string y) {
        res ~= "g(%s, %s)".format(x, y);
    }
    void h() {
        res ~= "h";
        localVar++; //  force to be a delegate
    }
    void j(double z) {
        res ~= "j(%s)".format(z);
        localVar++; //  force to be a delegate
    }

    assert (!c.isSet());
    assert (!c);
    c();

    c = &h;
    assert (c.isSet());
    assert (c);
    c();
    c();

    c = null;
    assert (!c.isSet());

    c = &f;
    c();

    c.set(&g, 7, "hello");
    c();

    c.set!g(8, "world");
    c();

    c.set(&j, 3.14f);
    c();

    c.set(&j, 7);
    c();

    assert(res == ["h", "h", "f", "g(7, hello)", "g(8, world)", "j(3.14)", "j(7)"]);
}
