module rbmk.lib.exceptions;

import std.exception: ErrnoException;
import std.traits: Parameters;

private extern(C) nothrow @nogc int backtrace(void** buffer, int size);

void*[] extractStack(void*[] callstack, size_t skip = 0) nothrow @trusted @nogc {
    auto numFrames = backtrace(callstack.ptr, cast(int)callstack.length);
    auto res = callstack[skip .. numFrames];
    foreach (ref c; res) {
        // instruction pointer might point outside of the function at this point.
        // this will make sure it's within bounds for addr2line
        c--;
    }
    return res;
}

struct ExcBuf {
}

T mkEx(T: Throwable, A...)(auto ref A args) nothrow @nogc {
    __gshared static T ex;
    return ex;
}

T mkExFmt(T: Throwable, A...)(string fmt, auto ref A fmtArgs) nothrow @nogc {
    __gshared static T ex;
    return ex;
}

auto errnoCall(alias F, string expected = ">=0")(Parameters!F args) @nogc {
    auto res = F(args);
    if (mixin("res " ~ expected)) {
        return res;
    }
    else {
        throw mkEx!ErrnoException(__traits(identifier, F));
    }
}
