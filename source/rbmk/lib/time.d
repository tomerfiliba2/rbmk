module rbmk.lib.time;

public import std.datetime: Clock, Duration, usecs, msecs, seconds;
import std.datetime: hnsecs;
import core.sys.posix.time: clock_gettime, CLOCK_MONOTONIC, timespec, nanosleep;
import rbmk.arch.x86: readTSC;


struct TscTimePoint {
    enum zero = TscTimePoint(0);
    enum min = TscTimePoint(long.min);
    enum max = TscTimePoint(long.max);

    private __gshared static timespec initialKernelReading;
    private __gshared static long initialTscReading;
    private __gshared static long _cyclesPerSecond;
    @("thread-local") private shared static long lastTsc;
    @("thread-local") private shared static ulong softCounter;
    @("thread-local") private shared static ulong refreshIntervalMask;

    long cycles;

    @property static TscTimePoint now() nothrow @nogc {
        return TscTimePoint(readTSC());
    }
    @property static cyclesPerSecond() nothrow @nogc {
        return _cyclesPerSecond;
    }

    //
    // reading the TSC is expensive; usually a strict monotonically-increasing value is enough
    //
    @property static TscTimePoint softNow() nothrow @nogc {
        import core.atomic;
        ulong counter = atomicOp!"+="(softCounter, 1) - 1;
        if ((counter & refreshIntervalMask) == 0) {
            auto oldVal = lastTsc;
            auto newVal = readTSC();
            if (newVal > oldVal) {
                // we may fail to update the global variable here, but this will make sure we never go back
                cas(&lastTsc, oldVal, newVal);
            }
            return TscTimePoint(newVal);
        }
        else {
            // executing this function will take more than one cycle, so we can rest assured we will
            // never out-pace a true TSC reading
            long tmp = atomicOp!"+="(lastTsc, 1);
            return TscTimePoint(tmp);
        }
    }

    /+static void setRefreshInterval(ulong interval) {
        import angstrom.lib.bits: intLog2;
        refreshIntervalMask = (1UL << intLog2(interval)) - 1;
    }+/

    static long toCycles(Duration dur) nothrow @nogc {
        long hns = dur.total!"hnsecs";
        return (hns / 10_000_000) * _cyclesPerSecond + ((hns % 10_000_000) * _cyclesPerSecond) / 10_000_000;
    }

    Duration toDuration() nothrow @nogc {
        return toDuration(cycles);
    }
    static Duration toDuration(ulong cycles) nothrow @nogc {
        return hnsecs((cycles / _cyclesPerSecond) * 10_000_000 + ((cycles % _cyclesPerSecond) * 10_000_000) / _cyclesPerSecond);
    }
    auto diff(string units)(TscTimePoint rhs) nothrow @nogc {
        static if (units == "cycles") {
            return cycles - rhs.cycles;
        }
        else static if (units == "usecs") {
            return (cycles - rhs.cycles) / (_cyclesPerSecond / 1_000_000);
        }
        else static if (units == "msecs") {
            return (cycles - rhs.cycles) / (_cyclesPerSecond / 1_000);
        }
        else static if (units == "seconds") {
            return double(cycles - rhs.cycles) / _cyclesPerSecond;
        }
        else {
            static assert (false, "Unkown unit " ~ units);
        }
    }

    shared static this() {
        import core.sys.posix.unistd: read, close;
        import core.sys.posix.fcntl: O_RDONLY, open;
        import std.string: indexOf;
        import std.exception: enforce, errnoEnforce;

        char[4096*10] buf = void;
        int fd = open("/proc/cpuinfo", O_RDONLY);
        errnoEnforce(fd > 0, "opening '/proc/cpuinfo' failed");
        scope(exit) close(fd);
        auto count = read(fd, buf.ptr, buf.sizeof);
        errnoEnforce(count > 0, "reading '/proc/cpuinfo' failed");
        auto cpuinfo = buf[0 .. count];
        enforce(cpuinfo.indexOf("constant_tsc") >= 0, "'constant_tsc' not supported");

        timespec sleepTime = {tv_sec: 0, tv_nsec: 1_000_000}; // 1 msec
        clock_gettime(CLOCK_MONOTONIC, &initialKernelReading);
        initialTscReading = readTSC();
        nanosleep(&sleepTime, null);

        adjustFrequency();
    }

    //
    // this function should be called periodically (every minute or so) to keep our frequency
    // on par with the kernel's
    //
    static void adjustFrequency() {
        timespec t1;
        clock_gettime(CLOCK_MONOTONIC, &t1);
        long tscNow = readTSC();
        long nsecs = (t1.tv_sec - initialKernelReading.tv_sec) * 1_000_000_000UL + (t1.tv_nsec - initialKernelReading.tv_nsec);
        long newCyclesPerSecond = cast(long)((tscNow - initialTscReading) / (nsecs / 1E9));

        _cyclesPerSecond = newCyclesPerSecond;
    }
}
