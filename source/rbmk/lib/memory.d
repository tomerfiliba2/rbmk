module rbmk.lib.memory;

import std.string: toStringz;
import core.memory: GC;
import std.exception: errnoEnforce;
import core.sys.posix.sys.mman;
import core.sys.linux.sys.mman: MAP_POPULATE, MAP_ANONYMOUS;
import core.sys.posix.unistd: unlink;


struct MmapFile {
    string filename;
    void[] buffer;

    void open(string filename, bool readWrite=false) {
        this.filename = filename;
    }

    void close() {
        if (buffer !is null) {
            munmap(buffer.ptr, buffer.length);
            buffer = null;
        }
    }

    void unlink() {
        if (filename !is null) {
            .unlink(toStringz(filename));
            filename = null;
        }
    }
}

struct MmapBuffer {
    void[] buffer;

    static MmapBuffer allocate(size_t size) {
        void* addr = mmap(null, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_POPULATE, -1, 0);
        errnoEnforce(addr != MAP_FAILED, "mmap");
        return MmapBuffer(addr[0 .. size]);
    }

    void free() {
        if (buffer !is null) {
            munmap(buffer.ptr, buffer.length);
            buffer = null;
        }
    }

    // override these to make them non-modifiable
    @property void* ptr() pure nothrow @nogc {return buffer.ptr;}
    @property size_t length() const pure nothrow @nogc {return buffer.length;}
    @disable void length(size_t newSize);

    alias buffer this;
}

struct MmapArray(T) {
    T[] elements;

    static MmapArray allocate(size_t length) {
        auto buffer = MmapBuffer.allocate(T.sizeof * length);
        elements = cast(T[])buffer.buffer;
    }
    void free() {
        if (elements !is null) {
            auto buffer = cast(void[])elements;
            munmap(buffer.ptr, buffer.length);
            elements = null;
        }
    }
    @disable void length(size_t newSize);

    alias elements this;
}
