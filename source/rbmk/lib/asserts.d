module rbmk.lib.asserts;

@trusted pure nothrow @nogc
void ASSERT(string fmt, string mod=__MODULE__, size_t line=__LINE__, T...)(bool cond, lazy T args) {
    if (cond) {
        return;
    }
    /+
    scope dg = deleate() {
        assert(false);
    };
    //(cast(void delegate() nothrow pure @nogc)dg)();
    +/
}

version (assert) {
    enum DBG_ASSERT_ENABLED = true;
    alias DBG_ASSERT = ASSERT;
}
else {
    enum DBG_ASSERT_ENABLED = false;
    //@trusted pure nothrow @nogc pragma(inline, true)
    //void DBG_ASSERT(string fmt, string mod=__MODULE__, size_t line=__LINE__, T...)(bool cond, scope auto ref lazy T args) {}
}
