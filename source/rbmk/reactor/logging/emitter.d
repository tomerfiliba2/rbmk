module rbmk.reactor.logging.emitter;

import rbmk.logging.logdesc: LogDesc;

struct LogChunk {
    static struct Header {
        ulong chunkId;
        ulong wallTime;
        ulong tscBase;
        uint offset;
    }

    enum CHUNK_SIZE = 64*1024*1024;

    Header header = void;
    ubyte[0] data;
}
