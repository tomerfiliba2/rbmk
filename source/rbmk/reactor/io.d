module rbmk.reactor.io;

static import unistd = core.sys.posix.unistd;
import core.sys.linux.epoll;
import core.sys.posix.fcntl;
static import errno = core.stdc.errno;
import std.exception: errnoEnforce;
import std.traits: Parameters;
import rbmk.lib.asserts: ASSERT, DBG_ASSERT;
import rbmk.lib.exceptions: errnoCall;
import rbmk.lib.memory: MmapArray;

enum EventMask: ubyte {
    NONE = 0x00,
    R    = 0x01,
    W    = 0x02,
    X    = 0x04,
    RW   = R | W,
}

align(1) private struct FDInfo {
align(1):
    enum uint INVALID_GENERATION = 0;
    __gshared static uint genCounter = 1;

    uint      generation;
    EventMask askedMask;
    EventMask recvdMask;
    ubyte[2]  _padding;

    private @property isValid() {
        return generation != INVALID_GENERATION;
    }

    private void invalidate() nothrow @nogc {
        generation = INVALID_GENERATION;
        askedMask = EventMask.NONE;
        recvdMask = EventMask.NONE;
    }
}

static assert (FDInfo.sizeof == ulong.sizeof);
private __gshared FDInfo[] fdTable;

struct FD {
    enum invalid = FD.init;

    private int _fd = -1;
    private uint generation = FDInfo.INVALID_GENERATION;

    static FD add(int fd) nothrow @nogc {
        ASSERT!"%s already exists"(!fdTable[fd].isValid(), fd);
        FDInfo.genCounter += 2;
        uint gen = FDInfo.genCounter;
        DBG_ASSERT!"generation is invalid"(gen != FDInfo.INVALID_GENERATION);
        fdTable[fd].generation = gen;
        return FD(fd, gen);
    }
    void close() @nogc {
        if (isValid) {
            fdTable[_fd].invalidate();
            errnoCall!(unistd.close)(_fd);
        }
        _fd = -1;
        generation = FDInfo.INVALID_GENERATION;
    }
    @property bool isValid() const nothrow @nogc {
        return _fd >= 0 && fdTable[_fd].generation == generation;
    }
    FDInfo* getInfo() nothrow @nogc {
        ASSERT!"using a stale FD"(isValid);
        return &fdTable[_fd];
    }
    @property int getFD() const nothrow @nogc {
        ASSERT!"using a stale FD"(isValid);
        return _fd;
    }

    @property EventMask recvdEvents() nothrow @nogc {
        return getInfo.recvdMask;
    }

    @property ulong toUlong() const nothrow @nogc {
        return _fd | (ulong(generation) << 32);
    }
    static FD fromUlong(ulong val) nothrow @nogc {
        return FD(cast(uint)val, cast(uint)(val >> 32));
    }

    auto call(alias F)(Parameters!F[1..$] args) {
        static assert (is(Parameters!F[0] == int));
        ASSERT!"using a stale FD"(isValid);
        return F(_fd, args);
    }
    auto checkedCall(alias F, string expected = ">=0")(Parameters!F[1..$] args) {
        static assert (is(Parameters!F[0] == int));
        ASSERT!"using a stale FD"(isValid);
        return errnoCall!(F, expected)(_fd, args);
    }

    int yieldCall(alias F)(Parameters!F[1..$] args) {
        Epoll.register(fd);
        while (true) {
            int res = fd.call!F(args);
            if (res == errno.EINTR) {
                continue;
            }
            else if (res == errno.EAGAIN) {
                Epoll.suspend(fd);
            }
            else {
                return res;
            }
        }
    }
}

static assert (FD.sizeof == ulong.sizeof);

struct Epoll {
static:
    __gshared FD epfd;

    void open() {
        ASSERT!"epfd already open"(!epfd.isValid, epfd);
        epfd = FD.add(errnoCall!epoll_create1(EPOLL_CLOEXEC));
    }
    void close() {
        epfd.close();
    }

    void register(FD fd) {
        FDInfo* info = fd.getInfo();
        if (info.askedMask != EventMask.NONE) {
            return;
        }

        auto flags = fd.checkedCall!fcntl(F_GETFL);
        if (!(flags & O_NONBLOCK)) {
            errnoEnforce(fcntl(fd._fd, F_SETFL, flags | O_NONBLOCK) >= 0, "fcntl(F_SETFL, O_NONBLOCK) failed");
        }

        epoll_event evt = {
            events: EPOLLET | EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLHUP | EPOLLPRI,
            data: {
                u64: fd.toUlong
            },
        };
        epfd.checkedCall!epoll_ctl(EPOLL_CTL_ADD, fd._fd, &evt);
        info.askedMask = EventMask.RW;
    }

    void unregister(FD fd) {
        if (!fd.isValid) {
            return;
        }
        FDInfo* info = fd.getInfo();
        if (info.askedMask == EventMask.NONE) {
            return;
        }
        epoll_event unused;
        epfd.checkedCall!epoll_ctl(EPOLL_CTL_DEL, fd._fd, &unused);
        info.askedMask = EventMask.NONE;
    }

    void processEvents(void delegate(FD) @nogc dg, int timeoutMsecs=-1) @nogc {
        epoll_event[256] evts = void;
        auto count = epfd.checkedCall!epoll_wait(evts.ptr, evts.length, timeoutMsecs);
        foreach(ref evt; evts[0 .. count]) {
            auto fd = FD.fromUlong(evt.data.u64);
            if (!fd.isValid) {
                //ERROR!""
                epoll_event unused;
                epfd.checkedCall!epoll_ctl(EPOLL_CTL_DEL, fd._fd, &unused);
                continue;
            }
            EventMask mask = EventMask.NONE;
            if (evt.events & (EPOLLIN | EPOLLRDHUP | EPOLLHUP | EPOLLPRI)) {
                mask |= EventMask.R;
            }
            if (evt.events & EPOLLOUT) {
                mask |= EventMask.W;
            }
            if (evt.events & EPOLLERR) {
                mask |= EventMask.X;
            }
            fd.getInfo.recvdMask |= mask;
            dg(fd);
        }
    }

    void suspend(FD fd) @nogc {
        import weka.reactor.scheduler: Scheduler;
        Scheduler.suspend();
    }
}
