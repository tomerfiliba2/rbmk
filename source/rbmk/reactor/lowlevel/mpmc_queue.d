module rbmk.reactor.lowlevel.mpmc_queue;

import core.atomic;
import core.thread: Thread;
import std.traits: isPointer, isIntegral;
import rbmk.arch.x86: xchg;


struct MPMCQueue(T = void*, T _invalidT = null, bool yieldWhenFull = false) {
    static assert (T.sizeof <= ulong.sizeof);
    static assert (isPointer!T || isIntegral!T);

    private enum ulong _invalid = cast(ulong)_invalidT;
    public enum T invalid = _invalidT;

    private align(64) {
        shared ulong[] arr;
        shared ulong ri;
        shared ulong wi;
        shared ulong queued;
    }

    public void reset(size_t count) nothrow {
        assert ((count & (count-1)) == 0);
        ri = wi = queued = 0;
        arr = new shared ulong[count];
        arr[] = _invalid;
    }

    public @property ulong numQueued() const pure nothrow {
        return queued;
    }

    public void push(T value) nothrow {
        ulong val = cast(ulong)value;
        assert (val != _invalid);
        while (true) {
            ulong idx = atomicOp!"+="(wi, 1) - 1;
            if (cas(&arr[idx & ($-1)], _invalid, val)) {
                atomicOp!"+="(queued, 1);
                break;
            }
            static if (yieldWhenFull) {
                Thread.yield();
            }
        }
    }

    public T pop() nothrow {
        while (queued > 0) {
            ulong idx = atomicOp!"+="(ri, 1) - 1;
            ulong val = xchg(&arr[idx & ($-1)], _invalid);
            if (val != _invalid) {
                atomicOp!"-="(queued, 1);
                return cast(T)val;
            }
        }
        return invalid;
    }
}

version(long_ut):
unittest {
    import std.stdio;
    import core.thread;
    import std.datetime;

    enum ulong EOF = long.max;

    void test(int queueSize, int numConsumerThreads, int numProducerThreads, ulong itemsPerProducer) {
        MPMCQueue!(ulong, ulong.max) queue;
        queue.reset(queueSize);

        Thread[] consumerThds;
        consumerThds.length = numConsumerThreads;
        Thread[] producerThds;
        producerThds.length = numProducerThreads;
        shared(ulong)[] consumerResults;
        consumerResults.length = numConsumerThreads;
        shared(ulong)[] consumerCounts;
        consumerCounts.length = numConsumerThreads;

        void consumer(size_t idx) {
            scope(failure) writefln("XXX C%s", idx);
            ulong accum;
            ulong count;
            //writefln("C%s starts", idx);

            while (true) {
                ulong res = queue.pop();
                if (res == queue.invalid) {
                    continue;
                }
                if (res == EOF) {
                    //writefln("C%s got EOF", idx);
                    break;
                }
                //writefln("C%s popped %s", idx, res);
                accum += cast(ulong)res;
                count++;
            }
            //writefln("C%s finished (accum=%s)", idx, accum);
            atomicStore(consumerResults[idx], accum);
            atomicStore(consumerCounts[idx], count);
        }

        void producer(size_t idx) {
            scope(failure) writefln("XXX P%s", idx);
            ulong start = (idx+1) * itemsPerProducer;
            ulong end = (idx+2) * itemsPerProducer;
            //writefln("P%s starts %s..%s", idx, start, end);
            foreach(i; start .. end) {
                //writefln("P%s pushing %s", idx, i);
                queue.push(i);
            }
            //writefln("P%s finished", idx);
        }

        writefln("\nQ=%s numConsumerThreads=%s numProducerThreads=%s itemsPerProducer=%s", queueSize, numConsumerThreads, numProducerThreads, itemsPerProducer);

        foreach(i, ref thd; consumerThds) {
            thd = new Thread(((i) => {consumer(i);})(i));
            thd.start();
        }
        foreach(i, ref thd; producerThds) {
            thd = new Thread(((i) => {producer(i);})(i));
            thd.start();
        }
        auto t0 = Clock.currStdTime();
        foreach(thd; producerThds) {
            thd.join();
        }
        //writefln("%s producers are done, sending EOF to %s consumers", producerThds.length, consumerThds.length);
        foreach(thd; consumerThds) {
            queue.push(EOF);
        }
        foreach(thd; consumerThds) {
            thd.join();
        }
        auto t1 = Clock.currStdTime();
        //writefln("waiting for consumers");

        ulong totalAccum = 0;
        foreach(i; 0 .. consumerResults.length) {
            totalAccum += atomicLoad(consumerResults[i]);
        }
        ulong expected;
        foreach(idx; 0 .. producerThds.length) {
            ulong start = (idx+1) * itemsPerProducer;
            ulong end = (idx+2) * itemsPerProducer;
            foreach(i; start .. end) {
                expected += i;
            }
        }

        double avg=0;
        foreach(cnt; consumerCounts) {
            avg+=cnt;
        }
        avg /= consumerCounts.length;
        double var=0;
        foreach(cnt; consumerCounts) {
            var += (cnt - avg)^^2;
        }
        var /= consumerCounts.length - 1;
        auto sd = var^^0.5;

        writefln("    totalAccum=%s expected=%s time=%ss counts=%s avg=%s sd=%s err=%.3f%%", totalAccum, expected,
            (t1 - t0) / 1e7, consumerCounts, avg, sd, 100*(sd / avg));
        assert (totalAccum == expected);
    }

    enum items = 1_000_000;

    // many to many
    test(256, 8, 32, items);
    test(256, 20, 20, items);

    // single-consumer/mutli-producer and vice versa
    test(4096, 32, 1, items);
    test(4096, 1, 32, items);
}
