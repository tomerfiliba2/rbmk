module rbmk.reactor.lowlevel.timer_queue;

import std.stdio;
import std.string;
import std.datetime: Duration;
import core.bitop: bsr;
import rbmk.containers.lists: IntrusiveList;

///
/// The LogarithmicTimerQueue is a super-efficient, absolutely accurate timer queue that can work
/// at any resolution, and coverts the whole 64-bit range. Inserting and canceling are O(1) worst-case
/// and popping is O(1) amortized, since the levels of the queue need to be unpacked as time passes.
/// The invariant the queue provides is that a timer needs to be moved at most `numLevels` times,
/// as each level is exponentailly larger than the one before it. numLevels is usually 8, and for
/// a timer to be moved more than 4 times is highly unlikely.
///
/// The queue works in arbitrary "time units", essentially 64 bit unsigned integers, which are mapped
/// internally to "ticks". Ticks are also arbitrary, but allow reducing the resolution of the input clock.
/// For example, the input clock may be the CPU's TSC counter, which is ticks at the CPU's frequency,
/// say 3GHz. You may want to scale it down so that the queue works in ticks of (up to) 50us --
/// instead of 0.33ns that the input clock provides. Under these conditions, 50us is 150000 cycles
/// so by choosing a ticksScaler of int(log2(150000)), i.e., 17, you can configure the queue to work
/// at a resolution of 44us (2^^17 cycles at 3GHz is 44us).
///
/// To illustrate the structure of the logarithmic queue, assuming bitsPerLevel=8:
///   level 0 addresses bins of individual ticks
///   level 1 addresses bins of 256 ticks each
///   level 2 addresses bins of 65536 ticks each
///   level 3 addresses bins of 16777216 ticks each
///   etc.
///
/// The timer `T` needs to provide only the following attributes:
///
///    ulong timePoint;
///    T* next;
///    T* prev;
///
/// Options for `bitsPerLevel`:
///  * bitsPerLevel = 1  --> 64 levels, each with 1 bin      (64 TimerLists)
///  * bitsPerLevel = 2  --> 32 levels, each with 4 bins     (128 TimerLists)
///  * bitsPerLevel = 4  --> 16 levels, each with 16 bins    (256 TimerLists)
///  * bitsPerLevel = 8  -->  8 levels, each with 256 bins   (2048 TimerLists)
///  * bitsPerLevel = 16 -->  4 levels, each with 65536 bins (262144 TimerLists)
///  * bitsPerLevel = 32 -->  2 levels, each with 4294967296 bins
///  * bitsPerLevel = 64 -->  1 level... you cannot hope to allocate so much memory anyway
///
/// Of these, only 4 and 8 offer a reasonable compromise between size and runtime behaviour (how many
/// level-unpackings take place). Use 4 if space is an issue, otherwise leave it at 8.
///
struct LogarithmicTimerQueue(T, ubyte bitsPerLevel=8) {
    static assert ((bitsPerLevel & (bitsPerLevel-1)) == 0, "bitsPerLevel must be a power of 2");
    enum binsPerLevel = 1 << bitsPerLevel;
    enum numLevels = (ulong.sizeof * 8) / bitsPerLevel;
    static assert (numLevels * bitsPerLevel == ulong.sizeof * 8);

private:
    ulong t0ticks;
    ulong tickCounter;
    ubyte ticksScaler;
    version(unittest) ulong reinserts;

    alias TimerList = IntrusiveList!T;
    TimerList expiredList;
    TimerList[binsPerLevel][numLevels] bins;

    public void setup(ulong now, ubyte ticksScaler = 0) nothrow @nogc {
        this.ticksScaler = ticksScaler;
        this.t0ticks = toTicks(now);
        this.tickCounter = 0;
    }
    public void setup(ulong now, double clockFreqHZ /* e.g., 3E9 */ , Duration desiredResolution /* e.g., 50.usecs */) {
        import std.math: log2;
        setup(now, cast(ubyte)log2(clockFreqHZ * (desiredResolution.total!"nsecs" / 1E9)));
    }

    @property ulong nowTicks() const pure nothrow @nogc {pragma(inline, true);
        return t0ticks + tickCounter;
    }
    auto indexAtLevel(ulong n, ubyte level) const pure nothrow @nogc {pragma(inline, true);
        return (n >> (bitsPerLevel*level)) % binsPerLevel;
    }
    auto baseIndexOf(ubyte level) const pure nothrow @nogc {pragma(inline, true);
        return (tickCounter >> (bitsPerLevel*level)) % binsPerLevel;
    }
    ulong toTicks(ulong timeUnits) const pure nothrow @nogc {pragma(inline, true);
        return timeUnits >> ticksScaler;
    }

    TimerList* locateBin(ulong ticks) nothrow @nogc {
        if (ticks < nowTicks) {
            return &expiredList;
        }
        else if (ticks == nowTicks) {
            return &bins[0][baseIndexOf(0)];
        }
        else {
            ulong diff = ticks - nowTicks;
            ubyte level = cast(ubyte)(bsr(diff) / bitsPerLevel);
            return &bins[level][indexAtLevel(tickCounter + diff, level)];
        }
    }

    /// inserts timer `t` to the queue; O(1)
    public void insert(T* t) nothrow @nogc {pragma(inline, true);
        locateBin(toTicks(t.timePoint)).append(t);
    }
    // removes timer `t` from the queue; O(1)
    public void remove(T* t) nothrow @nogc {pragma(inline, true);
        locateBin(toTicks(t.timePoint)).remove(t);
    }

    // returns true iff it's time to call pop()
    public bool shouldPop(ulong now) const nothrow @nogc {
        return !expiredList.empty || toTicks(now) > nowTicks;
    }

    // pops the next ready timer from the queue; amortized O(1)
    public T* pop(ulong now) nothrow @nogc {
        if (!expiredList.empty) {
            return expiredList.popHead();
        }

        auto newNowTicks = toTicks(now);
        while (nowTicks <= newNowTicks) {
            auto bin0 = &bins[0][baseIndexOf(0)];
            if (!bin0.empty) {
                return bin0.popHead();
            }
            tickCounter++;
            if (baseIndexOf(0) == 0) {
                unpackNext(1);
            }
        }

        return null;
    }

    void unpackNext(ubyte level) nothrow @nogc {
        auto baseIdx = baseIndexOf(level);
        unpackBin(&bins[level][baseIdx]);
        if (baseIdx == 0) {
            // a 64 bit counter shouldn't overflow for ~130 years given a 4GHz CPU frequency (even if ticksScaler = 0)
            // so we don't really need to handle wraparound
            assert (level < numLevels - 1, "tickCounter overflows");
            unpackNext(cast(ubyte)(level+1));
        }
    }

    void unpackBin(TimerList* bin) nothrow @nogc {
        while (!bin.empty) {
            auto t = bin.popHead();
            insert(t);
            version(unittest) reinserts++;
            // make sure we don't reinsert to the same bin, otherwise it's an infinite loop
            assert (locateBin(toTicks(t.timePoint)) !is bin);
        }
    }

    /// returns how many "time units" are left till the first timer,
    /// where 0 means you may pop() right now and `ulong.max` means the queue is empty
    public ulong timeTillFirstTimer() const nothrow @nogc {
        if (!expiredList.empty) {
            return 0;
        }
        ulong ticks = 0;
        foreach(ubyte level; 0 .. numLevels) {
            auto baseIdx = baseIndexOf(level);
            foreach(i; 0 .. binsPerLevel) {
                if (!bins[level][(i+baseIdx) % binsPerLevel].empty) {
                    return ticks >> ticksScaler;
                }
                ticks += 1 << (bitsPerLevel * level);
            }
        }
        return ulong.max;
    }
}

version(long_ut):
unittest {
    static struct Timer {
        ulong timePoint;
        Timer* next;
        Timer* prev;
    }

    import std.random;
    import std.algorithm: min;
    Random rnd;
    uint seed = unpredictableSeed(); writeln("seed=", seed);
    //uint seed = 1337;
    scope(failure) writeln("seed=", seed);

    rnd.seed(seed);
    //enum strict = true;

    enum ulong start = 111_111_111_111_111;
    //enum ulong end   = 111_111_151_111_111;
    enum ulong end   = 111_111_121_111_111;

    bool[Timer*] allTimers;
    ulong numInserts, numPops;

    LogarithmicTimerQueue!(Timer, 8) queue;
    queue.setup(start);

    void popAll(ulong now, bool strict) {
        bool any;
        while (true) {
            auto t = queue.pop(now);
            if (t is null) {
                break;
            }
            any = true;
            assert (t.timePoint <= now, "tp=%s now=%s queue.now=%s".format(t.timePoint, now, queue.nowTicks));
            assert (allTimers.remove(t));
            numPops++;
            //writefln("pop(%s) -> %s", now, t.timePoint);
        }
        if (!any) {
            //writefln("pop(%s) -> N/A", now);
        }
        if (strict) {
            foreach(t; allTimers.byKey) {
                assert (t.timePoint > now, "did not pop %s (now=%s queue.now=%s counter=%s)".format(
                    t.timePoint, now, queue.nowTicks, queue.tickCounter));
            }
        }
    }

    ulong now = start;
    ulong nextReport = 0;

    import core.memory: GC;
    GC.disable();
    scope(exit) GC.enable();

    enum numItems = 10000;
    enum slack = 1_000_000;

    while (now < end) {
        while (numInserts < numPops + numItems && now < end - slack) {
            //auto tp = uniform(now - 50, end, rnd);
            auto tp = uniform(now - 100, (numInserts % 8 == 0) ? end : min(now + slack, end), rnd);
            assert (tp <= end);
            auto t = new Timer(tp);
            allTimers[t] = true;
            queue.insert(t);
            numInserts++;
        }
        now += uniform(1, 100, rnd);
        popAll(now, (now % 16) == 0);

        if (now > nextReport) {
            nextReport = now + 1_000_000;
            writefln("[%s] inserts=%s pops=%s reinserts=%s", now, numInserts, numPops, queue.reinserts);
        }
    }

    writefln("now=%s numInserts=%s numPops=%s reinserts=%s", now, numInserts, numPops, queue.reinserts);
    assert (numInserts == numPops);
    assert (allTimers.length == 0);
}
