module rbmk.reactor.lowlevel.fibril;

import core.thread: Thread;
import rbmk.lib.asserts: ASSERT, DBG_ASSERT;
import rbmk.lib.types: roundDown, accessMemberType, accessMember;


version (D_InlineAsm_X86_64) version (Posix) {
    private pure nothrow @trusted @nogc
    void* _fibril_init_stack(void* stackTop, void function(void*) nothrow fn, void* opaque) {
        // set rsp to top of stack, and make sure it's 16-byte aligned
        auto rsp = cast(void*)roundDown!16(cast(ulong)stackTop);
        auto rbp = rsp;

        void push(void* v) nothrow pure @nogc {
            rsp -= v.sizeof;
            *(cast(void**)rsp) = v;
        }

        push(null);                     // Fake RET of entrypoint
        push(&_fibril_trampoline);      // RIP
        push(rbp);                      // RBP
        push(null);                     // RBX
        push(null);                     // R12
        push(null);                     // R13
        push(fn);                       // R14
        push(opaque);                   // R15

        return rsp;
    }

    enum INIT_STACK_SIZE = 8 * ulong.sizeof;

    pragma(inline, false) extern(C) private pure nothrow @trusted @nogc
    void _fibril_trampoline() {
        asm pure nothrow @nogc {
            naked;
            mov RDI, R14;  // fn
            mov RSI, R15;  // opaque

            // this has to be a jmp (not a call), otherwise exception-handling will see
            // this function in the stack and be... unhappy
            jmp _fibril_wrapper;
        }
    }

    pragma(inline, false) extern(C) private pure nothrow @trusted @nogc
    void _fibril_switch(void** fromRSP /* RDI */, void* toRSP /* RSI */) {
        asm pure nothrow @nogc {
            naked;

            // save current state, then store RSP into `fromRSP`
            // RET is already pushed at TOS (right before RBP)
            push RBP;
            push RBX;
            push R12;
            push R13;
            push R14;
            push R15;
            mov [RDI], RSP;

            // set RSP to `toRSP` and load state
            // and return to caller (RET will be at TOS after popping everything)
            mov RSP, RSI;
            pop R15;
            pop R14;
            pop R13;
            pop R12;
            pop RBX;
            pop RBP;
            ret;
        }
    }

    extern(C) private pragma(inline, false) nothrow
    void _fibril_wrapper(void function(void*) fn /* RDI */, void* opaque /* RSI */) {
        import core.stdc.stdlib: abort;
        import core.sys.posix.unistd: write;

        void writeErr(const(char[]) text) {
            write(2, text.ptr, text.length);     // Write error directly to stderr
        }

        try {
            fn(opaque);
            writeErr("Fibril function must never return\n");
        }
        catch (Throwable ex) {
            writeErr("Fibril function must never throw\n");
            try {ex.toString(&writeErr);} catch (Throwable) {}
            writeErr("\n");
        }
        // we abort twice so that there wouldn't be tail-call optimization and the function will remain on the stack
        // (useful for tracebacks)
        abort();
        abort();
        assert(false);
    }
}

private {
    alias StackContext = accessMemberType!(Thread, "m_main");
    pragma(mangle, "_D4core6thread6Thread7sm_mainCQBcQBaQw") extern __gshared Thread mainThread;

    extern(C) void* _d_eh_swapContext(void* newContext) nothrow @nogc;
    extern(C) void* _d_eh_swapContextDwarf(void* newContext) nothrow @nogc;

    pragma(mangle, "_D4core6thread6Thread3addFNbNiPSQBeQBcQy7ContextZv")
        void threadAddContext(StackContext*) nothrow @nogc;
    pragma(mangle, "_D4core6thread6Thread6removeFNbNiPSQBhQBfQBb7ContextZv")
        void threadRemoveContext(StackContext*) nothrow @nogc;

    pragma(inline, true) private void* threadSwapContext(void* newEhConext) nothrow @nogc {
        if (auto p = _d_eh_swapContext(newEhConext)) {
            return p;
        }
        else if (auto p = _d_eh_swapContextDwarf(newEhConext)) {
            return p;
        }
        else {
            return null;
        }
    }
}

struct Fibril {
    private StackContext* sc;

    pragma(inline, true) @property static Fibril main() nothrow @nogc {
        return Fibril(&(mainThread.accessMember!"m_main"()));
    }
    pragma(inline, true) @property static Fibril current() nothrow @nogc {
        return Fibril(mainThread.accessMember!"m_curr");
    }
    pragma(inline, true) @property bool isMainFibril() nothrow @nogc {
        return sc is main.sc;
    }

    // note: stackArea is expected to be zeroed out
    static Fibril create(void[] stackArea, void function(void*) nothrow fn, void* opaque,
                         size_t reservedSize) nothrow @nogc {
        ASSERT!"stack too small: %s (min is %s+%s+%s)"(stackArea.length > StackContext.sizeof + reservedSize + INIT_STACK_SIZE,
            stackArea.length, StackContext.sizeof, reservedSize, INIT_STACK_SIZE);

        StackContext* sc = cast(StackContext*)(stackArea.ptr + (stackArea.length - StackContext.sizeof - reservedSize));
        sc.bstack = stackArea.ptr + stackArea.length;
        sc.tstack = _fibril_init_stack(sc.bstack - (StackContext.sizeof + reservedSize), fn, opaque);
        sc.ehContext = null;
        threadAddContext(sc);
        return Fibril(sc);
    }

    void destroy() nothrow @nogc {
        ASSERT!"cannot unregister main fibril"(!isMainFibril);
        if (sc !is null) {
            threadRemoveContext(sc);
            sc = null;
        }
    }

    pragma(inline, true) @property void* reservedArea() nothrow @nogc {
        ASSERT!"cannot get reserved area of main fibril"(!isMainFibril);
        return (cast(void*)sc) + this.sizeof;
    }
    pragma(inline, true) static Fibril fromReservedArea(void* ptr) nothrow @nogc {
        return Fibril(cast(StackContext*)(ptr - this.sizeof));
    }

    void switchTo() nothrow @nogc {
        if (current.sc is this.sc) {
            return;
        }

        // note: we're taking a few shortcuts here since we rely on GC being disabled at all times
        // except when explictly enabled in the main fiber. also, we allow only the main thread to run fibers.
        // confer with `core.thread.Fiber.switchIn` for the full version

        StackContext* srcSC = mainThread.accessMember!"m_curr";
        StackContext* dstSC = this.sc;
        srcSC.ehContext = threadSwapContext(dstSC.ehContext);
        mainThread.accessMember!"m_curr" = dstSC;
        _fibril_switch(&srcSC.tstack, dstSC.tstack);
    }
}

unittest {
    import core.memory: GC;
    GC.disable();
    scope(exit) GC.enable();

    __gshared static Fibril fib1, fib2;
    __gshared static char[] result;

    static void fib1Func(void* opaque) {
        foreach(_; 0 .. 10) {
            result ~= "A";
            fib2.switchTo();
        }
        result ~= "X";
        Fibril.main.switchTo();
    }

    static void fib2Func(void* opaque) {
        while (true) {
            result ~= "B";
            fib1.switchTo();
        }
    }

    fib1 = Fibril.create(new ubyte[4096], &fib1Func, null, 0);
    scope(exit) fib1.destroy();

    fib2 = Fibril.create(new ubyte[4096], &fib2Func, null, 0);
    scope(exit) fib2.destroy();

    fib1.switchTo();

    assert (result == "ABABABABABABABABABABX", result);
}
