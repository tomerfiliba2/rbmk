module rbmk.reactor.sync;

import rbmk.lib.asserts: ASSERT, DBG_ASSERT;
import rbmk.reactor.scheduler: FiberHandle, Scheduler;
import rbmk.containers.lists: IntrusiveList;

struct FiberQueue {
    static struct Waiter {
        Waiter*     next;
        Waiter*     prev;
        FiberHandle fibHandle;
        void*       opaque;
    }
    private IntrusiveList!Waiter list;

    @disable this(this);

    ~this() {
        // XXX: go over list and kill all waiters?
        DBG_ASSERT!"FiberQueue not empty when being destroyed"(empty);
    }

    @property bool empty() const nothrow @nogc {
        return list.empty;
    }

    void suspendCaller(void* opaque = null) @nogc {
        // this variable sits on the stack and cannot move
        Waiter waiter;
        waiter.fibHandle = Scheduler.thisFiberHandle();
        waiter.opaque = opaque;
        list.append(&waiter);
        scope(exit) list.remove(&waiter);
        Scheduler.suspend();
    }

    FiberHandle resumeOne() nothrow @nogc {
        if (empty) {
            return FiberHandle.invalid;
        }

        Waiter* waiter = list.popHead();
        DBG_ASSERT!"popped an invalid waiter"(waiter.fibHandle.isValid);
        Scheduler.resume(waiter.fibHandle);
        return waiter.fibHandle;
    }

    void resumeAll() nothrow @nogc {
        while (!empty) {
            resumeOne();
        }
    }

    void* peekOpaque() nothrow @nogc {
        DBG_ASSERT!"queue is empty"(!empty);
        return list.head.opaque;
    }
}

struct Event {
    private FiberQueue waiters;
    private bool _isSet;

    void set() nothrow @nogc {
        _isSet = true;
        waiters.resumeAll();
    }
    void reset() nothrow @nogc {
        _isSet = false;
    }
    @property bool isSet() const pure nothrow @nogc {
        return _isSet;
    }

    void wait() @nogc {
        DBG_ASSERT!"waiting while in critical section"(!Scheduler.isInCriticalSection);
        if (!_isSet) {
            waiters.suspendCaller();
            // theoretically, loop where as the event may already be unset when we wake up
        }
    }
    void wait(string fmt, string mod=__MODULE__, uint lineNum=__LINE__, T...)(T args) {
        if (!_isSet) {
            DEBUG!(fmt, mod, lineNum, T)(args);
        }
        wait();
    }
}

struct ETEvent {
    private FiberQueue waiters;

    void wait() @nogc {
        waiters.suspendCaller();
    }
    void trigger() nothrow @nogc {
        waiters.resumeAll();
    }
}

struct Lock {
    private FiberQueue waiters;
    private FiberHandle owner;

    @property bool isLockedByCurrentFiber() const nothrow @nogc {
        return owner == Scheduler.thisFiberHandle;
    }

    bool tryAcquire() nothrow @nogc {
        if (owner.isValid) {
            return false;
        }

        DBG_ASSERT!"no owner but has waiters"(waiters.empty);
        owner = Scheduler.thisFiberHandle;
        return true;
    }

    void acquire() @nogc {
        DBG_ASSERT!"acquiring in a critical section"(!Scheduler.isInCriticalSection);
        if (tryAcquire()) {
            return;
        }

        scope(failure) {
            if (isLockedByCurrentFiber) {
                release();
            }
        }
        waiters.suspendCaller();
        DBG_ASSERT!"owner=%s"(isLockedByCurrentFiber, owner);
    }
    void release() nothrow @nogc {
        DBG_ASSERT!"owner=%s"(isLockedByCurrentFiber, owner);
        owner = waiters.resumeOne();
    }
}

struct ReentrantLock {
    private Lock lock;
    private size_t counter;

    @property bool isLockedByCurrentFiber() const nothrow @nogc {
        return lock.isLockedByCurrentFiber();
    }

    bool tryAcquire() nothrow @nogc {
        if (lock.isLockedByCurrentFiber) {
            counter++;
            return true;
        }
        if (lock.tryAcquire()) {
            DBG_ASSERT!"counter=%s"(counter == 0, counter);
            counter = 1;
            return true;
        }
        return false;
    }

    void acquire() {
        DBG_ASSERT!"acquiring in a critical section"(!Scheduler.isInCriticalSection);
        if (tryAcquire()) {
            return;
        }

        lock.acquire();
        DBG_ASSERT!"counter=%s"(counter == 0, counter);
        counter = 1;

    }
    void release() {
        DBG_ASSERT!"lockedByCurrent=%s counter=%s"(counter > 0 && lock.isLockedByCurrentFiber,
            counter, lock.isLockedByCurrentFiber);
        counter--;
        if (counter == 0) {
            lock.release();
        }
    }
}

auto owned(string acqMethod="acquire", string relMethod="release", T, ARGS...)(T* obj, ARGS args) @nogc {
    static struct RAII {
        private T* obj;
        private ARGS args;

        @disable this(this);
        this(T* obj, ARGS args) @nogc {
            __traits(getMethod, obj, acqMethod)(args);
            this.obj = obj;
            this.args = args;
        }

        ~this() nothrow @nogc {
            if (obj !is null) {
                __traits(getMethod, obj, relMethod)(args);
                obj = null;
            }
        }
    }
    return RAII(obj, arg);
}

struct RWLock {
    static struct Handle {
    private:
        public Handle* next, prev;
        FiberHandle fibHandle;
        RWLock* rwlock;
        bool isExclusive;
        bool granted;
        bool waited;

        @disable this(this);
        @disable void opPostMove(T)(ref T);
        @property bool isShared() const pure nothrow @nogc {return !isExclusive;}

        ~this() nothrow @nogc {
            if (granted) {
                release();
            }
            DBG_ASSERT!"rwlock=%s next=%s prev=%s"(rwlock is null && next is null && prev is null, rwlock, next, prev);
        }

        void enqueue(RWLock* rwlock, bool isExclusive) nothrow @nogc {
            DBG_ASSERT!"this.rwlock=%s next=%s prev=%s"(this.rwlock is null && next is null && prev is null,
                                                        this.rwlock, next, prev);
            DBG_ASSERT!"rwlock=%s"(rwlock !is null);

            this.fibHandle = Scheduler.thisFiberHandle;
            this.rwlock = rwlock;
            this.isExclusive = isExclusive;
            this.granted = false;
            this.waited = false;
            rwlock.waiters.pushTail(&this);
        }

        public bool tryLock() nothrow @nogc {
            DBG_ASSERT!"rwlock=%s granted=%s waited=%s"(rwlock !is null && !granted && !waited, rwlock, granted, waited);
            DBG_ASSERT!"no waiters"(!rwlock.waiters.empty);

            if (rwlock.waiters.head !is &this) {
                return false;
            }
            if (rwlock.owners.empty || (rwlock.owners.head.isShared && this.isShared)) {
                rwlock.waiters.remove(&this);
                granted = true;
                rwlock.owners.pushTail(&this);
                return true;
            }
            return false;
        }

        void wait() @nogc {
            DBG_ASSERT!"acquiring in a critical section"(!Scheduler.isInCriticalSection);
            if (tryLock()) {
                waited = true;
                return;
            }

            waited = true;
            scope(failure) {
                if (granted) {
                    release();
                }
                else {
                    rwlock.waiters.remove(&this);
                }
            }

            Scheduler.suspend();
            DBG_ASSERT!"not granted"(granted);
        }

        public void release() nothrow @nogc {
            DBG_ASSERT!"rwlock=%s granted=%s"(rwlock !is null && granted, rwlock, granted);

            scope(exit) rwlock = null;
            granted = false;
            rwlock.owners.remove(&this);

            while (!rwlock.waiters.empty) {
                Handle* nextHandle = rwlock.waiters.head;
                DBG_ASSERT!"nextReq is granted"(!nextHandle.granted);

                if (rwlock.owners.empty || (rwlock.owners.head.isShared && nextHandle.isShared)) {
                    nextHandle.granted = true;
                    rwlock.owners.pushTail(nextHandle);
                    Scheduler.resume(nextHandle.fibHandle);
                    continue;
                }
                break;
            }
        }
    }

    private IntrusiveList!Handle waiters;
    private IntrusiveList!Handle owners;

    ~this() {
        DBG_ASSERT!"rwlock destoyed with waiters=%s owners=%s"(waiters.empty && owners.empty,
            !waiters.empty, !owners.empty);
    }

    void asyncAcquireShared(Handle* handle) nothrow @nogc {
        handle.enqueue(&this, false);
    }
    void asyncAcquireExclusive(Handle* handle) nothrow @nogc {
        handle.enqueue(&this, true);
    }
    void acquireShared(Handle* handle) @nogc {
        handle.enqueue(&this, false);
        handle.wait();
    }
    void acquireExclusive(Handle* handle) @nogc {
        handle.enqueue(&this, true);
        handle.wait();
    }
}

//
// Strict-FIFO semaphore
//
struct Semaphore {
    FiberQueue waiters;
    ulong balance;

    this(ulong capacity) nothrow @nogc {
        reset(capacity);
    }
    void reset(ulong capacity) nothrow @nogc {
        this.balance = 0;
        if (capacity > 0) {
            release(capacity);
        }
    }

    bool tryAcquire(ulong count = 1) nothrow @nogc {
        DBG_ASSERT!"count=%s"(count > 0, count);
        if (balance >= count && waiters.empty) {
            balance -= count;
            return true;
        }
        return false;
    }

    void acquire(ulong count = 1) @nogc {
        DBG_ASSERT!"acquiring in a critical section"(!Scheduler.isInCriticalSection);
        if (tryAcquire(count)) {
            return;
        }

        ulong requested = count;

        scope (failure) {
            // release() will zero out the `requested` parameter if it woke us up. the following `if` handles the case
            // where we've been killed, still hadn't been switched-in (so we're still in the FiberQueue), and release()
            // also wished to wake us up, consuming budget for us. in this case, return the wrongfully-consumed budget
            if (requested == 0) {
                release(count);
            }
        }

        waiters.suspendCaller(&requested);
        DBG_ASSERT!"requested=%s"(requested == 0, requested);
    }

    void release(ulong count = 1) nothrow @nogc {
        DBG_ASSERT!"count=0"(count > 0);
        balance += count;
        while (!waiters.empty) {
            ulong* requested = cast(ulong*)waiters.peekOpaque();
            if (balance < *requested) {
                break;
            }

            balance -= *requested;
            *requested = 0;
            waiters.resumeOne();
        }
    }
}

//
// blocks "master" fiber until all "worker" fibers are done
//
struct Barrier {
    Semaphore sem = Semaphore(0);

    void markDone() nothrow @nogc {
        sem.release(1);
    }
    void waitFor(ulong count) @nogc {
        sem.acquire(count);
    }
}
