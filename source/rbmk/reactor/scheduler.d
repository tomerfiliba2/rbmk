module rbmk.reactor.scheduler;

import core.memory: GC;
import std.exception: errnoEnforce;
import std.typecons: Flag, Yes, No;

import rbmk.lib.asserts: ASSERT, DBG_ASSERT;
import rbmk.lib.memory: MmapBuffer;
import rbmk.lib.exceptions: errnoCall, ExcBuf;
import rbmk.lib.closure: Closure;
import rbmk.lib.types: roundUp, bitFlag, bitField;
import rbmk.containers.lists: IntrusiveList;
import rbmk.reactor.lowlevel.fibril: Fibril;


struct Fiber {
    enum Owner: ubyte {
        NONE,
        FREELIST,
        SCHEDLIST,
    }

    Fibril        fibril;
    Fiber*        next;
    Fiber*        prev;
    uint          flags;
    Closure       closure;
    //ExcBuf        excBuf;
    Throwable     ex;

    alias owner        = bitField!(Owner, flags, 0, 2);
    alias hasException = bitFlag!(flags, 3);
    alias hasStarted   = bitFlag!(flags, 4);

    static Fiber* create(void[] stackArea) nothrow @nogc {
        auto fibril = Fibril.create(stackArea, &wrapper, null, this.sizeof);
        Fiber* fib = cast(Fiber*)fibril.reservedArea();
        fib.fibril = fibril;
        return fib;
    }
    void destroy() nothrow @nogc {
        fibril.destroy();
    }

    pragma(inline, true) static @property Fiber* current() nothrow @nogc {
        return cast(Fiber*)Fibril.current.reservedArea;
    }

    void prefetch() nothrow @nogc {
        import rbmk.arch.x86: prefetch;
        prefetch!2(&this);
    }

    void extractStack() {
    }


    void wrapper(void* opaque) nothrow {
        while (true) {
            try {
                ASSERT!"hasStarted=%s closure.isSet=%s"(!hasStarted && closure.isSet, hasStarted, closure.isSet);
                hasStarted = true;
                closure();
                Scheduler.recycleFiber();
            }
            catch (FiberKilled) {
                // treated like normal fiber termination
                Scheduler.recycleFiber();
            }
            catch (Throwable ex) {
                Scheduler.recycleFiber(ex);
            }
        }
    }

    void switchTo() @nogc {
        fibril.switchTo();
    }
}

struct FiberHandle {
    enum invalid = FiberHandle(null);
    Fiber* _fib;
    @property bool isValid() const nothrow @nogc {
        return _fib !is null;
    }
}

class SchedulerControl: Throwable {
    this(string msg){super(msg);}
}
class SchedulerShutdown: SchedulerControl {
    __gshared static singleton = new typeof(this)();
    this(){super(this.stringof);}
}
class CollectGarbage: SchedulerControl {
    __gshared static singleton = new typeof(this)();
    this(){super(this.stringof);}
}
class FiberKilled: Throwable {
    __gshared static singleton = new typeof(this)();
    this(){super(this.stringof);}
}

struct Scheduler {
__gshared static:
    bool _started;
    uint criticalSectionNesting;
    void[] stackArea;
    Fiber*[] allFibers;
    IntrusiveList!Fiber freeList;
    IntrusiveList!Fiber scheduledList;
    Fiber mainFiber;

    void open(size_t fibStackSize, size_t numFibers) {
        import rbmk.arch.linux: SYS_PAGE_SIZE;
        import core.sys.posix.sys.mman: mprotect, mmap, MAP_PRIVATE, MAP_FAILED, MAP_ANON,
                                        PROT_NONE, PROT_READ, PROT_WRITE;

        allFibers.length = numFibers;
        const totalFibStack = roundUp!SYS_PAGE_SIZE(fibStackSize) + SYS_PAGE_SIZE;
        const totalSize = totalFibStack * numFibers + SYS_PAGE_SIZE;

        // create a single VMA for the stacks and punch guard pages into it
        void* addr = mmap(null, totalSize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);
        errnoEnforce(addr != MAP_FAILED, "mmap(stackArea)");
        stackArea = addr[0 .. totalSize];

        errnoCall!mprotect(stackArea.ptr, SYS_PAGE_SIZE, PROT_NONE);
        void* stackPtr = stackArea.ptr + SYS_PAGE_SIZE;

        mainFiber.fibril = Fibril.main;

        foreach(ref fib; allFibers) {
            void[] fibStack = stackPtr[0 .. totalFibStack - SYS_PAGE_SIZE];
            void* guardPage = stackPtr + totalFibStack - SYS_PAGE_SIZE;
            stackPtr += totalFibStack;

            // XXX maybe munlock() the guard page?
            errnoCall!mprotect(guardPage, SYS_PAGE_SIZE, PROT_NONE);
            fib = Fiber.create(fibStack);
            freeList.pushTail(fib);
        }
    }

    void close() {
        import core.sys.posix.sys.mman: munmap;
        foreach(fib; allFibers) {
            fib.destroy();
        }
        stackArea.length = 0;
        errnoCall!munmap(stackArea.ptr, stackArea.length);
        stackArea = null;
    }

    pragma(inline, true) static @property bool isOnMainFiber() nothrow @nogc {
        return Fiber.current is &mainFiber;
    }

    Fiber* getNextFiber() nothrow @nogc {
        /+if (nextFiber !is null) {
            auto fib = nextFiber;
            nextFiber = null;
            return fib;
        }+/
        if (!scheduledList.empty) {
            auto fib = scheduledList.popHead();
            fib.owner = Fiber.Owner.NONE;
            return fib;
        }
        return &mainFiber;
    }

    void suspend() @nogc {
        // in the scope of `current` fiber
        {
            ASSERT!"Suspending when criticalSectionNesting=%s"(criticalSectionNesting == 0, criticalSectionNesting);
            Fiber* curr = Fiber.current;
            Fiber* next = getNextFiber();
            next.prefetch();
            next.switchTo();
        }
        // we've switched stacks. we're now in the scope of `next` fiber
        {
            Fiber* curr = Fiber.current;
            if (curr.ex) {
                auto ex = curr.ex;
                curr.ex = null;
                throw ex;
            }
        }
    }

    void yield() {
        resume(Fiber.current);
        suspend();
    }

    void resume(Fiber* fib) nothrow @nogc {
        final switch (fib.owner) {
            case Fiber.Owner.FREELIST:
                ASSERT!"resuming a freelist fiber"(false);
                assert(false);
            case Fiber.Owner.SCHEDLIST:
                return;
            case Fiber.Owner.NONE:
                scheduledList.pushTail(fib);
                break;
        }
    }

    FiberHandle thisFiberHandle() nothrow @nogc {
        return FiberHandle(Fiber.current);
    }

    void resume(FiberHandle handle) nothrow @nogc {
    }

    void enterCriticalSection() nothrow @nogc  {
        criticalSectionNesting++;
    }
    void leaveCriticalSection() nothrow @nogc  {
        ASSERT!"criticalSectionNesting=0"(criticalSectionNesting > 0);
        criticalSectionNesting--;
    }
    @property bool isInCriticalSection() nothrow @nogc {
        return criticalSectionNesting > 0;
    }

    void switchToMain() nothrow @nogc {
    }

    void throwInFiber(Fiber* fib, Throwable ex, Flag!"immediate" immediate = No.immediate) @nogc {
        if (Fiber.current is fib) {
            throw ex;
        }
        fib.ex = ex;
        if (immediate) {
            switchToFibAndBack(fib);
        }
    }

    void switchToFibAndBack(Fiber* fib) @nogc {
        if (Fiber.current.owner == Fiber.Owner.SCHEDLIST) {
            scheduledList.remove(Fiber.current);
        }
        if (fib.owner == Fiber.Owner.SCHEDLIST) {
            scheduledList.remove(fib);
        }
        Fiber.current.owner = Fiber.Owner.SCHEDLIST;
        fib.owner = Fiber.Owner.SCHEDLIST;
        scheduledList.pushHead(Fiber.current);
        scheduledList.pushHead(fib);
        suspend();
    }

    void throwInMain(Throwable ex, Flag!"immediate" immediate = No.immediate) @nogc {
        throwInFiber(&mainFiber, ex, immediate);
    }

    void recycleFiber(Throwable ex = null) nothrow @nogc {
        Fiber* fib = Fiber.current;
        DBG_ASSERT!"recycling main fiber"(!isOnMainFiber);

        if (fib.owner == Fiber.Owner.SCHEDLIST) {
            scheduledList.remove(fib);
        }
        fib.flags = 0;
        fib.closure = null;
        fib.owner = Fiber.Owner.FREELIST;
        freeList.pushHead(fib);

        try {
            if (ex !is null) {
                throwInMain(ex, Yes.immediate);
            }
            else {
                suspend();
            }
        }
        catch (Throwable ex) {
            import core.sys.posix.stdlib: abort;
            abort();
        }
    }

    void collectGarbage() {
        throwInMain(CollectGarbage.singleton, Yes.immediate);
    }

    void stop() @nogc {
        throwInMain(SchedulerShutdown.singleton, Yes.immediate);
    }
    void start() {
        ASSERT!"Already started"(!_started);
        _started = true;
        scope(exit) _started = false;

        GC.collect();
        GC.disable();
        scope(exit) GC.enable();

        while (true) {
            try {
                suspend();
            }
            catch (CollectGarbage) {
                GC.enable();
                scope(exit) GC.disable();
                GC.collect();
            }
            catch (SchedulerShutdown) {
                break;
            }

            //pollerFunc();
        }
    }
}
