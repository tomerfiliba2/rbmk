module rbmk.reactor.net;

import core.sys.posix.sys.socket: AF_UNSPEC, AF_INET, AF_INET6, AF_UNIX, sa_family_t,
                                  socklen_t, sockaddr, socket, bind, listen, SOCK_STREAM;
import core.sys.posix.netinet.in_: in_addr, in_addr_t, in6_addr, in_port_t, htons,
                                   ntohs, sockaddr_in, sockaddr_in6, IPPROTO_TCP;
import core.sys.posix.arpa.inet: INET_ADDRSTRLEN, INET6_ADDRSTRLEN, inet_pton;
import core.sys.posix.sys.un: sockaddr_un;

import rbmk.lib.exceptions: errnoCall;
import rbmk.reactor.io: FD;


struct IP4 {
    enum ADDRSTRLEN = INET_ADDRSTRLEN;

    enum any = IP4(0, 0, 0, 0);
    enum invalid = IP4(0, 0, 0, 0);
    enum broadcast = IP4(0xff, 0xff, 0xff, 0xff);

    ubyte[in_addr_t.sizeof] bytes;

    this(ubyte a, ubyte b, ubyte c, ubyte d) nothrow @nogc {
        bytes = [a, b, c, d];
    }
    this(string str) nothrow @nogc {
        char[ADDRSTRLEN+1] strz = void;
        strz[0 .. str.length] = str;
        strz[str.length] = '\0';
        int res = inet_pton(AF_INET, strz.ptr, bytes.ptr);
        assert (res == 1, str);
    }
    @property ref inout(in_addr) as_in_addr() inout nothrow @nogc {
        return *(cast(inout(in_addr)*)bytes.ptr);
    }
}

struct IP6 {
    enum ADDRSTRLEN = INET6_ADDRSTRLEN;

    enum invalid = IP6.init;

    static assert (in6_addr.sizeof == 16);
    ubyte[16] bytes;

    this(ubyte[16] bytes) nothrow @nogc {
        this.bytes = bytes;
    }
    this(string str) {
        char[ADDRSTRLEN+1] strz = void;
        strz[0 .. str.length] = str;
        strz[str.length] = '\0';
        int res = inet_pton(AF_INET6, strz.ptr, bytes.ptr);
        assert (res == 1, str);
    }

    @property ref inout(in6_addr) as_in6_addr() inout nothrow @nogc {
        return *(cast(inout(in6_addr)*)bytes.ptr);
    }
}

struct IPPort {
    ushort portNBO;

    this(ushort portHBO) {
        portNBO = htons(portHBO);
    }

    @property ushort hostOrder() const pure nothrow @nogc {
        return ntohs(portNBO);
    }
    @property ref inout(in_port_t) as_in_port() inout nothrow @nogc {
        return *(cast(inout(in_port_t)*)&portNBO);
    }
}

struct SockAddr {
    enum maxLength = sun.sizeof;

    socklen_t length = maxLength;
    union {
        sa_family_t   family = AF_UNSPEC;
        sockaddr      sa;
        sockaddr_in   sin4;
        sockaddr_in6  sin6;
        sockaddr_un   sun;
    }

    this(IP4 ip, IPPort port) {
        length = sin4.sizeof;
        sin4.sin_family = AF_INET;
        sin4.sin_port = port.as_in_port;
        sin4.sin_addr = ip.as_in_addr;
    }
    this(IP6 ip, IPPort port) {
        length = sin6.sizeof;
        sin6.sin6_family = AF_INET6;
        sin6.sin6_port = port.as_in_port;
        sin6.sin6_addr = ip.as_in6_addr;
    }
    static SockAddr unixDomain(string path) {
        SockAddr sa;
        //assert (path.length < sa.sun.sun_path.length);
        sa.length = sun.sizeof;
        sa.sun.sun_family = AF_UNIX;
        sa.sun.sun_path[0 .. path.length] = cast(byte[])path;
        sa.sun.sun_path[path.length] = 0;
        return sa;
    }

    @property inout(sockaddr)* as_sockaddr() inout nothrow @nogc {
        return &sa;
    }
}

struct BaseSocket {
    FD fd;

    this(int domain, int type, int protocol) {
        fd = FD.add(errnoCall!socket(domain, type, protocol));
    }
    void bind(const ref SockAddr sockAddr) {
        fd.checkedCall!(.bind)(sockAddr.as_sockaddr, sockAddr.length);
    }
    void close() {
        fd.close();
    }
}

struct ListenerSocket {
    BaseSocket base;
    alias base this;

    static ListenerSocket listen(const ref SockAddr sockAddr, int type, int protocol) {
        auto sock = ListenerSocket(BaseSocket(sockAddr.family, type, protocol));
        sock.base.bind(sockAddr);
        sock.base.fd.checkedCall!(.listen)(10);
        return sock;
    }

    static ListenerSocket listenTCP(const ref SockAddr sockAddr) {
        return listen(sockAddr, SOCK_STREAM, IPPROTO_TCP);
    }

    ConnectedSocket accept(ref SockAddr sa) {
        sa.length = sa.maxLength;
        //fd.yieldRead!(.accept)(sa.as_sockaddr(), &sa.length);
        //return ConnectedSocket(fd);
        return ConnectedSocket.init;
    }
}

struct ConnectedSocket {
    BaseSocket base;
    alias base this;

    void connect(const ref SockAddr sockAddr, int type, int protocol) @nogc {

    }
    void connectTCP(const ref SockAddr sockAddr) {
        return connect(sockAddr, SOCK_STREAM, IPPROTO_TCP);
    }
}

struct DatagramSocket {
    BaseSocket base;
    alias base this;
}
