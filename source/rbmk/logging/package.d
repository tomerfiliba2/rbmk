module rbmk.logging;

import std.string: splitLines, join;
import rbmk.logging.logdesc: LOG, LogType;

pragma(inline, true) nothrow @nogc @safe {
    void DEBUG(string fmt, string mod=__MODULE__, uint lineNum=__LINE__, T...)(auto ref T args) {
        LOG!(LogType.DEBUG, fmt, mod, lineNum, T)(args);
    }

    void INFO(string fmt, string mod=__MODULE__, uint lineNum=__LINE__, T...)(auto ref T args) {
        LOG!(LogType.INFO, fmt, mod, lineNum, T)(args);
    }

    void WARN(string fmt, string mod=__MODULE__, uint lineNum=__LINE__, T...)(auto ref T args) {
        LOG!(LogType.WARN, fmt, mod, lineNum, T)(args);
    }

    void ERROR(string fmt, string mod=__MODULE__, uint lineNum=__LINE__, T...)(auto ref T args) {
        LOG!(LogType.ERROR, fmt, mod, lineNum, T)(args);
    }

    void META(string fmt, string mod=__MODULE__, uint lineNum=__LINE__, T...)(auto ref T args) {
        LOG!(LogType.META, fmt, mod, lineNum, T)(args);
    }

    void EXCEPTION(string fmt, string mod=__MODULE__, uint lineNum=__LINE__, T...)(Throwable ex, auto ref T args) {
        LOG!(LogType.EXCEPTION, fmt, mod, lineNum, T)(args);
    }
}

enum traced = q{
    import std.traits: __ParameterIdentifierTuple = ParameterIdentifierTuple;
    import rbmk.logging.logdesc: __LOG = LOG, __LogType = LogType;
	alias __thisFunc = __traits(parent, {});
    enum __thisFuncParams = __ParameterIdentifierTuple!__thisFunc;

    static if (__thisFuncParams.length == 0) {
	    __LOG!(__LogType.FN_ENTER, __FUNCTION__ ~ "()", __MODULE__, __LINE__)();
    }
    else {
        enum __paramsFmt = (){
            string s;
            foreach(name; __thisFuncParams) {
                s ~= name ~ " = %s, ";
            }
            return s[0 .. $-2];
        }();
        enum __paramsStr = (){
            string s;
            foreach(name; __thisFuncParams) {
                s ~= name ~ ", ";
            }
            return s[0 .. $-2];
        }();

        mixin(`__LOG!(__LogType.FN_ENTER, __FUNCTION__ ~ "(` ~ __paramsFmt ~ `)", __MODULE__, __LINE__)(` ~ __paramsStr ~ `);`);
    }

    scope(failure) __LOG!(__LogType.FN_THROW, __FUNCTION__, __MODULE__, __LINE__)();
	scope(success) __LOG!(__LogType.FN_RETURN, __FUNCTION__, __MODULE__, __LINE__)();
}.splitLines().join("");
