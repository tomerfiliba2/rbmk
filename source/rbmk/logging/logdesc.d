module rbmk.logging.logdesc;

import std.string: format;
import rbmk.logging.typedesc: cstring, internString, getTypeDesc, TypeRef;
version(LDC) {
    import ldc.attributes: section, assumeUsed, weak;
}
else {
    private struct section {string s;}
    private struct assumeUsed {}
    private struct weak {}
}

public enum LogType: ubyte {
    DEBUG,
    INFO,
    WARN,
    ERROR,
    META,

    // with traceback
    EXCEPTION,
    STACK,

    // functions
    FN_ENTER,
    FN_RETURN,
    FN_THROW,
}

public align(64) struct LogDesc {
    LogType   type;
    uint      lineNum;
    cstring   mod;
    cstring   fmt;
    TypeRef   argTypes;
}

static assert ((LogDesc.sizeof & (LogDesc.sizeof-1)) == 0,
    "LogDesc.sizeof must be a power of 2, not %s".format(LogDesc.sizeof));
static assert (LogDesc.sizeof == 64, LogDesc.sizeof);

version(LDC) {
    public extern(C) extern __gshared static LogDesc __start_logdesc;
    public extern(C) extern __gshared static LogDesc __end_logdesc;
}

private TypeRef getArgsDesc(ARGS...)() {
    /*align(1)*/ static struct ArgsStruct {
    align(1):
        ARGS args;
    }
    return getTypeDesc!ArgsStruct;
}

//public alias EntryEmitter = void function(const(LogDesc)* desc, const(ubyte)[] argsBuf) nothrow @safe @nogc;
//public @("thread-local") EntryEmitter emitLog = (desc, argsBuf){};
extern(C) extern void emitLog(const(LogDesc)* desc, const(ubyte)[] argsBuf) nothrow @safe @nogc;

pragma(inline, false) nothrow @nogc @trusted public
void LOG(LogType type, string fmt, string mod, uint lineNum, ARGS...)(auto ref ARGS args) {
    align(1) static struct ArgsBuf {
    align(1):
        static foreach(i, T; ARGS) {
            mixin("ubyte[%s] _%s = void;".format(T.sizeof, i));
        }
    }

    @assumeUsed @section("logdesc") __gshared static immutable(LogDesc) logDesc = {
        type:     type,
        mod:      internString!mod,
        lineNum:  lineNum,
        fmt:      internString!fmt,
        argTypes: getArgsDesc!ARGS,
    };

    ArgsBuf argsBuf;
    static foreach(i; 0 .. ARGS.length) {
        argsBuf.tupleof[i] = *cast(ubyte[ARGS[i].sizeof]*)(&args[i]);
    }
    emitLog(&logDesc, (cast(ubyte*)&argsBuf)[0 .. ARGS.length == 0 ? 0 : ArgsBuf.sizeof]);
}
