module rbmk.logging.console_emitter;

import rbmk.logging.logdesc: LogDesc, LogType;

void consoleEmitter(const(LogDesc)* desc, const(ubyte)[] argsBuf) @safe nothrow @nogc {
    static void assumeNothrowNogc(scope void delegate() dg) @trusted nothrow @nogc {
        (cast(void delegate() nothrow @nogc)dg)();
    }

    assumeNothrowNogc({
        import std.stdio;
        import std.string: fromStringz;
        import std.datetime: Clock;

        auto t = Clock.currTime();
        __gshared static char[128] spaces = ' ';
        __gshared static int nesting;

        if (desc.type == LogType.FN_RETURN || desc.type == LogType.FN_THROW) {
            nesting--;
        }

        writefln("%02d:%02d:%02d.%06d | %s:%3s | %s%s%s %s", t.hour, t.minute, t.second, t.fracSecs.total!"usecs",
            desc.mod.fromStringz, desc.lineNum, spaces[0 .. nesting * 3],
            desc.type == LogType.FN_ENTER ? "-> " : (desc.type == LogType.FN_RETURN ? "<- " : ""),
            fromStringz(desc.fmt), argsBuf/*desc.argTypes.dump(argsBuf)*/);

        if (desc.type == LogType.FN_ENTER) {
            nesting++;
        }
    });
}
