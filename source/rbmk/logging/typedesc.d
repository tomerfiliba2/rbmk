module rbmk.logging.typedesc;

import std.string: format, splitLines, join, indexOf, fromStringz;
version(LDC) {
    import ldc.attributes: section, assumeUsed;
}
else {
    private struct section {string s;}
    private struct assumeUsed {}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

package alias cstring = immutable(char)*;

private template _internString(string s) {
    static assert (s.indexOf("\x00") < 0, "string must not contain NUL");
    @assumeUsed @section("internstr") __gshared immutable(char)[s.length+1] _internString = s ~ "\x00";
}

package cstring internString(string s)() {
    return _internString!(s).ptr;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

package alias TypeRef = immutable(TypeDesc)*;

public struct TypeDesc {
    enum Kind: ubyte {
        BUILTIN       = 1,
        ENUM          = 2,
        STRUCT        = 3,
        UNION         = 4,
        STATIC_ARRAY  = 5,
        DYNAMIC_ARRAY = 6,
        //ASSOC_ARRAY,
        //CLASS,
    }

    Kind    kind;
    uint    size;
    cstring name;

    enum BuiltinDesc: ubyte {
        char_, bool_, byte_, ubyte_, short_, ushort_, int_, uint_, long_, ulong_, float_, double_,
    }

    static struct EnumDesc {
        static struct Member {
            cstring name;
            ulong   value;
        }
        TypeRef  baseType;
        uint     numMembers;
        immutable(Member)* pMember;

        @property immutable(Member)[] members() const pure nothrow @nogc {
            return pMember[0 .. numMembers];
        }
    }

    static struct StructDesc {
        static struct Member {
            cstring name;
            TypeRef type;
            uint    offset;
            uint    alignment;
        }

        uint numMembers;
        immutable(Member)* pMember;

        @property immutable(Member)[] members() const pure nothrow @nogc {
            return pMember[0 .. numMembers];
        }
    }

    static struct ArrayDesc {
        TypeRef type;
        size_t  length;
    }

    //union { -- cannot be a union, crashes the compiler
    BuiltinDesc      builtinDesc;
    EnumDesc         enumDesc;
    StructDesc       structDesc;
    ArrayDesc        arrayDesc;
    //}
}

private TypeDesc generateBuiltIn(T)() {
    TypeDesc typeDesc = {
        kind: TypeDesc.Kind.STRUCT,
        size: T.sizeof,
        name: internString!(T.stringof),
        builtinDesc: __traits(getMember, TypeDesc.BuiltinDesc, T.stringof ~ "_"),
    };
    return typeDesc;
}

private template generateEnumMembersDesc(T) {
    mixin(`
    @assumeUsed @section("typedesc") __gshared static
    immutable(TypeDesc.EnumDesc.Member)[__traits(allMembers, T).length] generateEnumMembersDesc = [` ~
    (){
        auto s = "";
        static foreach(name; __traits(allMembers, T)) {
            s ~= `{
                name:  "%s",
                value: __traits(getMember, T, "%s"),
            },`.format(name, name);
        }
        return s;
    }() ~ `];`);
}

private TypeDesc generateEnum(T)() {
    import std.traits: OriginalType;
    TypeDesc typeDesc = {
        kind: TypeDesc.Kind.ENUM,
        size: T.sizeof,
        name: internString!(T.stringof),
        enumDesc: {
            baseType:   getTypeDesc!(OriginalType!T),
            numMembers: __traits(allMembers, T).length,
            pMember:    generateEnumMembersDesc!T.ptr,
        }
    };
    return typeDesc;
}

private template generateStructMembersDesc(T) {
    mixin(`
    @assumeUsed @section("typedesc") __gshared static
    immutable(TypeDesc.StructDesc.Member)[T.tupleof.length] generateStructMembersDesc = [` ~
    (){
        auto s = "";
        static foreach(i, _; typeof(T.tupleof)) {
            s ~= `{
                name:      internString!(__traits(identifier, T.tupleof[%s])),
                type:      getTypeDesc!(typeof(T.tupleof[%s])),
                offset:    T.tupleof[%s].offsetof,
                alignment: T.tupleof[%s].alignof,
            },`.format(i, i, i, i);
        }
        return s;
    }() ~ `];`);
}

private immutable(TypeDesc.StructDesc.Member)* getStructMembersDesc(T)() {
    return generateStructMembersDesc!(T).ptr;
}

private TypeDesc generateStruct(T)() {
    assert (__ctfe);
    TypeDesc typeDesc = {
        kind: TypeDesc.Kind.STRUCT,
        size: T.sizeof,
        name: internString!(T.stringof),
        structDesc: {
            numMembers: T.tupleof.length,
            pMember:    generateStructMembersDesc!T.ptr,
        }
    };
    return typeDesc;
}

package template generateTypeDesc(T) {
    @assumeUsed @section("typedesc") __gshared static immutable(TypeDesc) generateTypeDesc = (){
        import std.traits: isIntegral, isBoolean, isSomeChar, isFloatingPoint, fullyQualifiedName,
                           isStaticArray, isDynamicArray;

        static if (is(T == enum)) {
            return generateEnum!T();
        }
        else static if (isIntegral!T || is(T == bool) || isSomeChar!T || isFloatingPoint!T) {
            return generateBuiltIn!T();
        }
        else static if (is(T == struct)) {
            return generateStruct!T();
        }
        else static if (is(T == union)) {
            return generateUnion!T();
        }
        else static if (isStaticArray!T) {
            return generateStaticArray!T();
        }
        else static if (isDynamicArray!T) {
            return generateDynamicArray!T();
        }
        else {
            static assert(false, "Cannot generate a TypeDesc for " ~ fullyQualifiedName!T);
        }
    }();
}

public TypeRef getTypeDesc(T)() {
    import std.traits: Unqual;
    return &generateTypeDesc!(Unqual!T);
}
